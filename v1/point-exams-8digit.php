<?php

$csv = array_map('str_getcsv', file('export.csv'));


$level = 9;
$school_id =1;

$out=array();

$answer = '
{"1":"D","2":"D","3":"D","4":"D","5":"D","6":"D","7":"A","8":"A","9":"A","10":"A","11":"C","12":"C","13":"C","14":"C","15":"C","16":"C","17":"D","18":"D","19":"D","20":"D","21":"B","22":"B","23":"B","24":"B","25":"B","26":"B","27":"C","28":"C","29":"C","30":"C","31":"A","32":"A","33":"A","34":"A","35":"A","36":"A","37":"B","38":"B","39":"B","40":"B","41":"D","42":"D","43":"D","44":"D","45":"D","46":"D","47":"C","48":"C","49":"C","50":"C","51":"B","52":"B","53":"B","54":"B","55":"B","56":"B","57":"A","58":"A","59":"A","60":"A","61":"A","62":"A","63":"A","64":"A","65":"A","66":"A","67":"B","68":"B","69":"B","70":"B","71":"A","72":"A","73":"A","74":"A","75":"A","76":"A","77":"B","78":"B","79":"B","80":"B","81":"B","82":"B","83":"B","84":"B","85":"B","86":"B","87":"C","88":"C","89":"C","90":"C"}
';

/*  */
   
   
function getData($a){
	return array(
		'olom_count'=>20,
		'motaleat_count'=>10,
		'zaban_count'=>7,
		'riazi_count'=>25,
		'farsi_count'=>10,
		'arabi_count'=>8,
		'qoran_count'=>10
	);
}

function avg($foo) {
	return round(array_sum($foo) / count($foo),2);
}

function stdev($precent,$arr,$average) {
	$num = count($arr);
	$sum2 = 0;
	foreach($arr as $val) {
	  $sum2 += pow(($val - $average), 2);
	}
	$stdev = sqrt($sum2 / ($num));

	return round((($precent-$average) / $stdev )  * 1000 + 5000);
}
	
function calcTrue($score,$skill){
		
		$count = getData('count');
		
		$num = 1;
		
		foreach($count as $k=>$v) {
			if($k==$skill)
				break;
			$num+=$v;
		}
		
		$out = 0;
		$to = $num+$count[$skill]-1;
		for($i=$num;$i<=$to ;$i++)
			if($score[$i]==1)
				$out++;

		return $out;
}
	
function calcFalse($score,$skill){	
	$count = getData('count');
	
	$num = 1;
	
	foreach($count as $k=>$v) {
		if($k==$skill)
			break;
		$num+=$v;
	}
	
	$out = 0;
	$to = $num+$count[$skill]-1;
	for($i=$num;$i<=$to ;$i++)
		if($score[$i]==-1)
			$out++;

	return $out;
}
	
function calcEmpty($score,$skill){
		
	$count = getData('count');
	
	$num = 1;
	
	foreach($count as $k=>$v) {
		if($k==$skill)
			break;
		$num+=$v;
	}
	
	$out = 0;
	$to = $num+$count[$skill]-1;
	for($i=$num;$i<=$to ;$i++)
		if($score[$i]==0)
			$out++;

	return $out;
}
	
function calcPercent($arrays,$skill){

		$true = $arrays['true'][$skill . '_true'];
		
		$false = $arrays['false'][$skill . '_false'];
		
		$count =  getData('count');
		
		$out = round(((($true * 3) - $false) / (3 * $count[$skill . '_count'] ) ) * 100,2);
		/*if($out<0)
			$out = '<span style="color:red">'.$out.'</span>';*/
		return $out;
}
	

$sql = "
	SET @answers = '".$answer."';
	INSERT INTO `exams` (`exams_id`, `name`, `date`, `answer` ,`level`,`school_id`) 
	VALUES (NULL, 'نام آزمون', 'تاریخ آزمون',@answers,'".$level."','".$school_id."');
	SET @exam_id = LAST_INSERT_ID();
";

$line = '';

echo $sql;

$percent_all = $tarazData = array();

for($i=1;$i<count($csv);$i++){

	$out = str_getcsv($csv[$i][0], ";");
	
	$rowN = count($out)-8;

	$sn = str_pad($out[$rowN].$out[$rowN+1].$out[$rowN+2].$out[$rowN+3].
		  $out[$rowN+4].$out[$rowN+5].$out[$rowN+6].$out[$rowN+7], 10, "0", STR_PAD_LEFT);
		
	unset($out[0],
		$out[$rowN],$out[$rowN+1],$out[$rowN+2],$out[$rowN+3],$out[$rowN+4],
		$out[$rowN+5],$out[$rowN+6],$out[$rowN+7]
	);
	
	$sheet_answer = json_decode($answer,true);
	
	$student_answer = array();
	
	foreach($sheet_answer as $k=>$v) {
		if($out[$k]=='') {
			$student_answer[$k] = 0;
		} else if($v==$out[$k]) {
			$student_answer[$k] = 1;
		} else {
			$student_answer[$k] = -1;
		}

	}
	
	$arrays = array();
	
	$arrays['true'] = array(
		'olom_true'=>calcTrue($student_answer,'olom_count'),
		'motaleat_true'=>calcTrue($student_answer,'motaleat_count'),
		'zaban_true'=>calcTrue($student_answer,'zaban_count'),
		'riazi_true'=>calcTrue($student_answer,'riazi_count'),
		'farsi_true'=>calcTrue($student_answer,'farsi_count'),
		'arabi_true'=>calcTrue($student_answer,'arabi_count'),
		'qoran_true'=>calcTrue($student_answer,'qoran_count'),
	);
	
	$arrays['false'] = array(
		'olom_false'=>calcFalse($student_answer,'olom_count'),
		'motaleat_false'=>calcFalse($student_answer,'motaleat_count'),
		'zaban_false'=>calcFalse($student_answer,'zaban_count'),
		'riazi_false'=>calcFalse($student_answer,'riazi_count'),
		'farsi_false'=>calcFalse($student_answer,'farsi_count'),
		'arabi_false'=>calcFalse($student_answer,'arabi_count'),
		'qoran_false'=>calcFalse($student_answer,'qoran_count'),
	);
	
	
	$arrays['empty'] = array(
		'olom_empty'=>calcEmpty($student_answer,'olom_count'),
		'motaleat_empty'=>calcEmpty($student_answer,'motaleat_count'),
		'zaban_empty'=>calcEmpty($student_answer,'zaban_count'),
		'riazi_empty'=>calcEmpty($student_answer,'riazi_count'),
		'farsi_empty'=>calcEmpty($student_answer,'farsi_count'),
		'arabi_empty'=>calcEmpty($student_answer,'arabi_count'),
		'qoran_empty'=>calcEmpty($student_answer,'qoran_count'),
	);
	
	$arrays['percent'] = array(
		'olom_percent'=>calcPercent($arrays,'olom'),
		'motaleat_percent'=>calcPercent($arrays,'motaleat'),
		'zaban_percent'=>calcPercent($arrays,'zaban'),
		'riazi_percent'=>calcPercent($arrays,'riazi'),
		'farsi_percent'=>calcPercent($arrays,'farsi'),
		'arabi_percent'=>calcPercent($arrays,'arabi'),
		'qoran_percent'=>calcPercent($arrays,'qoran'),
	);
	
	$tarazData[$sn] = $arrays['percent'];
	
	$percent_all['olom'][] = $arrays['percent']['olom_percent'];
	$percent_all['motaleat'][] = $arrays['percent']['motaleat_percent'];
	$percent_all['zaban'][] = $arrays['percent']['zaban_percent'];
	$percent_all['riazi'][] = $arrays['percent']['riazi_percent'];
	$percent_all['farsi'][] = $arrays['percent']['farsi_percent'];
	$percent_all['arabi'][] = $arrays['percent']['arabi_percent'];
	$percent_all['qoran'][] = $arrays['percent']['qoran_percent'];
	
	$line = "INSERT INTO `points` (
		`points_id`, `exams_id`, 
		`sn`,`code`,`answer`,
		`scores`,`olom`,`motaleat`,
		`zaban`,`riazi`,`farsi`,
		`arabi`,`qoran`
	) VALUES (
		NULL, 
		@exam_id,
		'".$sn."', 
		'".json_encode($out)."',
		'".json_encode($student_answer)."',
		'".json_encode($arrays)."',
		'".$arrays['percent']['olom_percent']."',
		'".$arrays['percent']['motaleat_percent']."',
		'".$arrays['percent']['zaban_percent']."',
		'".$arrays['percent']['riazi_percent']."',
		'".$arrays['percent']['farsi_percent']."',
		'".$arrays['percent']['arabi_percent']."',
		'".$arrays['percent']['qoran_percent']."'
	);\r\n";
	
	echo $line;
	
	$sql.=$line;
}

$line = "UPDATE `exams` SET 
olom_avg='".avg($percent_all['olom'])."',
motaleat_avg='".avg($percent_all['motaleat'])."',
zaban_avg='".avg($percent_all['zaban'])."',
riazi_avg='".avg($percent_all['riazi'])."',
farsi_avg='".avg($percent_all['farsi'])."',
arabi_avg='".avg($percent_all['arabi'])."',
qoran_avg='".avg($percent_all['qoran'])."'
WHERE(exams_id=@exam_id);\r\n\r\n";

echo $line;

$sql.= $line;

foreach($tarazData as $k=>$v){
	
	$line="UPDATE `points` SET
		`olom_taraz`='".stdev($v['olom_percent'],$percent_all['olom'],avg($percent_all['olom']))."', 
		`motaleat_taraz`='".stdev($v['motaleat_percent'],$percent_all['motaleat'],avg($percent_all['motaleat']))."', 
		`zaban_taraz`='".stdev($v['zaban_percent'],$percent_all['zaban'],avg($percent_all['zaban']))."', 
		`riazi_taraz`='".stdev($v['riazi_percent'],$percent_all['riazi'],avg($percent_all['riazi']))."', 
		`farsi_taraz`='".stdev($v['farsi_percent'],$percent_all['farsi'],avg($percent_all['farsi']))."', 
		`arabi_taraz`='".stdev($v['arabi_percent'],$percent_all['arabi'],avg($percent_all['arabi']))."', 
		`qoran_taraz`='".stdev($v['qoran_percent'],$percent_all['qoran'],avg($percent_all['qoran']))."'
		WHERE(
			exams_id=@exam_id AND 
			sn='".$k."'
		);\r\n\r\n
	";
	
	echo  $line;
	$sql.= $line;
}



$sql.= "UPDATE `points`
left join `students` using(sn)
set
	`points`.class = `students`.class,
	`points`.level = `students`.level";

file_put_contents('point-exams.sql', $sql);
?>