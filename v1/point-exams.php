<?php

$level = 9;
$school_id =1;


$csv = array_map('str_getcsv', file('export'.$level.'.csv'));



$out=array();

 $ans = array();
 
//2283523478
$ans['7'] = '{"1":"C","2":"B","3":"A","4":"D","5":"B","6":"C","7":"B","8":"A","9":"D","10":"D","11":"C","12":"A","13":"B","14":"D","15":"A","16":"C","17":"B","18":"D","19":"C","20":"D","21":"A","22":"B","23":"B","24":"D","25":"A","26":"B","27":"C","28":"B","29":"B","30":"C","31":"C","32":"C","33":"A","34":"C","35":"B","36":"B","37":"D","38":"A","39":"B","40":"A","41":"B","42":"A","43":"D","44":"B","45":"B","46":"C","47":"A","48":"D","49":"C","50":"C","51":"D","52":"B","53":"C","54":"B","55":"C","56":"D","57":"C","58":"A","59":"A","60":"B","61":"B","62":"C","63":"C","64":"C","65":"B","66":"A","67":"B","68":"A","69":"C","70":"A","71":"B","72":"D","73":"C","74":"A","75":"C","76":"D","77":"D","78":"C","79":"A","80":"A","81":"C","82":"A","83":"B","84":"B","85":"D","86":"A","87":"D","88":"B","89":"A","90":"A","91":"","92":"","93":"","94":"","95":"","96":"","97":"","98":"","99":"","100":"","101":"","102":"","103":"","104":"","105":"","106":"","107":"","108":"","109":"","110":"","111":"","112":"","113":"","114":"","115":"","116":"","117":"","118":"","119":"","120":""}
';

//2741942927
$ans['8'] = '{"1":"C","2":"C","3":"B","4":"B","5":"A","6":"A","7":"C","8":"D","9":"C","10":"C","11":"C","12":"B","13":"A","14":"A","15":"B","16":"B","17":"C","18":"A","19":"D","20":"C","21":"D","22":"C","23":"C","24":"C","25":"D","26":"B","27":"C","28":"D","29":"B","30":"D","31":"D","32":"C","33":"C","34":"A","35":"D","36":"B","37":"C","38":"C","39":"B","40":"B","41":"C","42":"A","43":"C","44":"A","45":"B","46":"D","47":"D","48":"A","49":"C","50":"A","51":"C","52":"B","53":"B","54":"C","55":"B","56":"C","57":"A","58":"C","59":"A","60":"A","61":"C","62":"C","63":"B","64":"C","65":"C","66":"D","67":"C","68":"C","69":"D","70":"B","71":"C","72":"A","73":"A","74":"C","75":"D","76":"A","77":"B","78":"D","79":"B","80":"C","81":"D","82":"C","83":"B","84":"D","85":"D","86":"B","87":"B","88":"D","89":"D","90":"D","91":"","92":"","93":"","94":"","95":"","96":"","97":"","98":"","99":"","100":"","101":"","102":"","103":"","104":"","105":"","106":"","107":"","108":"","109":"","110":"","111":"","112":"","113":"","114":"","115":"","116":"","117":"","118":"","119":"","120":"D"}
';

//2680303512
$ans['9'] = '{"1":"A","2":"C","3":"D","4":"B","5":"A","6":"B","7":"B","8":"C","9":"A","10":"B","11":"C","12":"C","13":"B","14":"B","15":"B","16":"B","17":"D","18":"D","19":"C","20":"B","21":"B","22":"C","23":"D","24":"D","25":"A","26":"A","27":"D","28":"A","29":"B","30":"B","31":"D","32":"B","33":"B","34":"A","35":"C","36":"B","37":"A","38":"C","39":"C","40":"A","41":"A","42":"C","43":"C","44":"A","45":"D","46":"C","47":"B","48":"B","49":"B","50":"D","51":"C","52":"A","53":"A","54":"A","55":"C","56":"D","57":"A","58":"B","59":"B","60":"D","61":"B","62":"C","63":"C","64":"D","65":"C","66":"D","67":"A","68":"C","69":"A","70":"B","71":"A","72":"A","73":"B","74":"B","75":"C","76":"D","77":"C","78":"A","79":"D","80":"C","81":"B","82":"C","83":"C","84":"D","85":"B","86":"C","87":"B","88":"D","89":"D","90":"A","91":"","92":"","93":"","94":"","95":"","96":"","97":"","98":"","99":"","100":"","101":"","102":"","103":"","104":"","105":"","106":"","107":"","108":"","109":"","110":"","111":"","112":"","113":"","114":"","115":"","116":"","117":"","118":"","119":"","120":""}';

$answer =  $ans[$level];
   
   
function getData($a){
	return array(
		'olom_count'=>20,
		'motaleat_count'=>10,
		'zaban_count'=>7,
		'riazi_count'=>25,
		'farsi_count'=>10,
		'arabi_count'=>8,
		'qoran_count'=>10
	);
}

function avg($foo) {
	return round(array_sum($foo) / count($foo),2);
}

function stdev($precent,$arr,$average) {
	$num = count($arr);
	$sum2 = 0;
	foreach($arr as $val) {
	  $sum2 += pow(($val - $average), 2);
	}
	$stdev = sqrt($sum2 / ($num));

	return round((($precent-$average) / $stdev )  * 1000 + 5000);
}
	
function calcTrue($score,$skill){
		
		$count = getData('count');
		
		$num = 1;
		
		foreach($count as $k=>$v) {
			if($k==$skill)
				break;
			$num+=$v;
		}
		
		$out = 0;
		$to = $num+$count[$skill]-1;
		for($i=$num;$i<=$to ;$i++)
			if($score[$i]==1)
				$out++;

		return $out;
}
	
function calcFalse($score,$skill){	
	$count = getData('count');
	
	$num = 1;
	
	foreach($count as $k=>$v) {
		if($k==$skill)
			break;
		$num+=$v;
	}
	
	$out = 0;
	$to = $num+$count[$skill]-1;
	for($i=$num;$i<=$to ;$i++)
		if($score[$i]==-1)
			$out++;

	return $out;
}
	
function calcEmpty($score,$skill){
		
	$count = getData('count');
	
	$num = 1;
	
	foreach($count as $k=>$v) {
		if($k==$skill)
			break;
		$num+=$v;
	}
	
	$out = 0;
	$to = $num+$count[$skill]-1;
	for($i=$num;$i<=$to ;$i++)
		if($score[$i]==0)
			$out++;

	return $out;
}
	
function calcPercent($arrays,$skill){

		$true = $arrays['true'][$skill . '_true'];
		
		$false = $arrays['false'][$skill . '_false'];
		
		$count =  getData('count');
		
		$out = round(((($true * 3) - $false) / (3 * $count[$skill . '_count'] ) ) * 100,2);
		/*if($out<0)
			$out = '<span style="color:red">'.$out.'</span>';*/
		return $out;
}
	

$sql = "
	SET @answers = '".$answer."';
	INSERT INTO `exams` (`exams_id`, `name`, `date`, `answer` ,`level`,`school_id`) 
	VALUES (NULL, 'نام آزمون', 'تاریخ آزمون',@answers,'".$level."','".$school_id."');
	SET @exam_id = LAST_INSERT_ID();
";

$line = '';

echo $sql;

$percent_all = $tarazData = array();

for($i=1;$i<count($csv);$i++){

	$out = str_getcsv($csv[$i][0], ";");
	
	$rowN = count($out)-10;
	
	$sn = $out[$rowN].$out[$rowN+1].$out[$rowN+2].$out[$rowN+3].
		  $out[$rowN+4].$out[$rowN+5].$out[$rowN+6].$out[$rowN+7].$out[$rowN+8].$out[$rowN+9];
		   
	$sn = strtr($sn,array('A'=>'0','B'=>'1','C'=>'2','D'=>'3',
	'E'=>'4','F'=>'5','G'=>'6','H'=>'7','I'=>'8','J'=>'9'));
		
	unset($out[0],
		$out[$rowN],$out[$rowN+1],$out[$rowN+2],$out[$rowN+3],$out[$rowN+4],
		$out[$rowN+5],$out[$rowN+6],$out[$rowN+7],$out[$rowN+8],$out[$rowN+9]
	);
	
	$sheet_answer = json_decode($answer,true);
	
	$student_answer = array();
	
	foreach($sheet_answer as $k=>$v) {
		if($out[$k]=='') {
			$student_answer[$k] = 0;
		} else if($v==$out[$k]) {
			$student_answer[$k] = 1;
		} else {
			$student_answer[$k] = -1;
		}

	}
	
	$arrays = array();
	
	$arrays['true'] = array(
		'olom_true'=>calcTrue($student_answer,'olom_count'),
		'motaleat_true'=>calcTrue($student_answer,'motaleat_count'),
		'zaban_true'=>calcTrue($student_answer,'zaban_count'),
		'riazi_true'=>calcTrue($student_answer,'riazi_count'),
		'farsi_true'=>calcTrue($student_answer,'farsi_count'),
		'arabi_true'=>calcTrue($student_answer,'arabi_count'),
		'qoran_true'=>calcTrue($student_answer,'qoran_count'),
	);
	
	$arrays['false'] = array(
		'olom_false'=>calcFalse($student_answer,'olom_count'),
		'motaleat_false'=>calcFalse($student_answer,'motaleat_count'),
		'zaban_false'=>calcFalse($student_answer,'zaban_count'),
		'riazi_false'=>calcFalse($student_answer,'riazi_count'),
		'farsi_false'=>calcFalse($student_answer,'farsi_count'),
		'arabi_false'=>calcFalse($student_answer,'arabi_count'),
		'qoran_false'=>calcFalse($student_answer,'qoran_count'),
	);
	
	
	$arrays['empty'] = array(
		'olom_empty'=>calcEmpty($student_answer,'olom_count'),
		'motaleat_empty'=>calcEmpty($student_answer,'motaleat_count'),
		'zaban_empty'=>calcEmpty($student_answer,'zaban_count'),
		'riazi_empty'=>calcEmpty($student_answer,'riazi_count'),
		'farsi_empty'=>calcEmpty($student_answer,'farsi_count'),
		'arabi_empty'=>calcEmpty($student_answer,'arabi_count'),
		'qoran_empty'=>calcEmpty($student_answer,'qoran_count'),
	);
	
	$arrays['percent'] = array(
		'olom_percent'=>calcPercent($arrays,'olom'),
		'motaleat_percent'=>calcPercent($arrays,'motaleat'),
		'zaban_percent'=>calcPercent($arrays,'zaban'),
		'riazi_percent'=>calcPercent($arrays,'riazi'),
		'farsi_percent'=>calcPercent($arrays,'farsi'),
		'arabi_percent'=>calcPercent($arrays,'arabi'),
		'qoran_percent'=>calcPercent($arrays,'qoran'),
	);
	
	$tarazData[$sn] = $arrays['percent'];
	
	$percent_all['olom'][] = $arrays['percent']['olom_percent'];
	$percent_all['motaleat'][] = $arrays['percent']['motaleat_percent'];
	$percent_all['zaban'][] = $arrays['percent']['zaban_percent'];
	$percent_all['riazi'][] = $arrays['percent']['riazi_percent'];
	$percent_all['farsi'][] = $arrays['percent']['farsi_percent'];
	$percent_all['arabi'][] = $arrays['percent']['arabi_percent'];
	$percent_all['qoran'][] = $arrays['percent']['qoran_percent'];
	
	$line = "INSERT INTO `points` (
		`points_id`, `exams_id`, 
		`sn`,`code`,`answer`,
		`scores`,`olom`,`motaleat`,
		`zaban`,`riazi`,`farsi`,
		`arabi`,`qoran`
	) VALUES (
		NULL, 
		@exam_id,
		'".$sn."', 
		'".json_encode($out)."',
		'".json_encode($student_answer)."',
		'".json_encode($arrays)."',
		'".$arrays['percent']['olom_percent']."',
		'".$arrays['percent']['motaleat_percent']."',
		'".$arrays['percent']['zaban_percent']."',
		'".$arrays['percent']['riazi_percent']."',
		'".$arrays['percent']['farsi_percent']."',
		'".$arrays['percent']['arabi_percent']."',
		'".$arrays['percent']['qoran_percent']."'
	);\r\n";
	
	echo $line;
	
	$sql.=$line;
}

$line = "UPDATE `exams` SET 
olom_avg='".avg($percent_all['olom'])."',
motaleat_avg='".avg($percent_all['motaleat'])."',
zaban_avg='".avg($percent_all['zaban'])."',
riazi_avg='".avg($percent_all['riazi'])."',
farsi_avg='".avg($percent_all['farsi'])."',
arabi_avg='".avg($percent_all['arabi'])."',
qoran_avg='".avg($percent_all['qoran'])."'
WHERE(exams_id=@exam_id);\r\n\r\n";

echo $line;

$sql.= $line;

foreach($tarazData as $k=>$v){
	
	$line="UPDATE `points` SET
		`olom_taraz`='".stdev($v['olom_percent'],$percent_all['olom'],avg($percent_all['olom']))."', 
		`motaleat_taraz`='".stdev($v['motaleat_percent'],$percent_all['motaleat'],avg($percent_all['motaleat']))."', 
		`zaban_taraz`='".stdev($v['zaban_percent'],$percent_all['zaban'],avg($percent_all['zaban']))."', 
		`riazi_taraz`='".stdev($v['riazi_percent'],$percent_all['riazi'],avg($percent_all['riazi']))."', 
		`farsi_taraz`='".stdev($v['farsi_percent'],$percent_all['farsi'],avg($percent_all['farsi']))."', 
		`arabi_taraz`='".stdev($v['arabi_percent'],$percent_all['arabi'],avg($percent_all['arabi']))."', 
		`qoran_taraz`='".stdev($v['qoran_percent'],$percent_all['qoran'],avg($percent_all['qoran']))."'
		WHERE(
			exams_id=@exam_id AND 
			sn='".$k."'
		);\r\n\r\n
	";
	
	echo  $line;
	$sql.= $line;
}



$sql.= "UPDATE `points`
left join `students` using(sn)
set
	`points`.class = `students`.class,
	`points`.level = `students`.level";

file_put_contents('point-exams.sql', $sql);
?>