<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */
 
namespace engine;

class router
{
	private $registry;
	private $path;
	private $args = array();
	private $accessLevel;
	private $loginCtrler;
	public  $file;
	public  $controller;
	public  $action; 

	function __construct($registry)
	{
        $this->registry = $registry;
		
		$this->registry->AccessID = $this->getUserAccess();
	}

	public function loader()
	{	
		/*** check the route ***/
		$this->getController();

		/*** a new controller class instance ***/
		$class 		= 'controller\\' . $this->controller;

		if (file_exists(SYSTEM_PATH . '/' . SITE_ROOT . '/model/'. $this->controller . '.php')) {
			
			$model 	= 'model\\' . $this->controller;
			$model = new $model($this->registry);
		} else {
		
			$model = NULL;
		}

		$controller = new $class($this->registry, $model);

		/*** check if the action is callable ***/
		$action = is_callable(array($controller, $this->action)) ? $this->action : 'index' ;

		/*** run the action ***/
		$controller->$action();
	}

	private function getController()
	{
		/*** get the route from the url ***/
		$route = (empty($_GET['r'])) ? '' : $_GET['r'];

		if (empty($route)) {
			$route = 'index';
		} else {
			/*** get the parts of the route ***/
			$parts = explode('/', $route);
			$this->controller = $parts[0];
				
			if(isset($parts[1]))
				$this->action = $parts[1];
		};

		if (empty($this->controller))
			$this->controller = 'index';

		/*** Get action ***/
		if (empty($this->action))
			$this->action = 'index';
			

		/** direct page to login conroller if not acceable **/
		if (!empty($this->accessLevel)) {

			if(!$this->registry->AccessID)
				$this->controller = $this->loginCtrler;
				
			if($this->registry->AccessID != $this->accessLevel)
				$this->controller = $this->loginCtrler;
		};
	}

	public function setAccess($accessLevel ,$loginCtrler)
	{
		$this->accessLevel = $accessLevel;
		$this->loginCtrler = $loginCtrler;
	}
	
	private function getUserAccess()
	{
		//return "Administrator";
		if (   preg_match('/^([0-9]{8,10})$/', $_SESSION[studentUsername])
			&& preg_match('/^([a-z0-9]{32})$/', $_SESSION[login_hash])
		) {
			$sql = 'SELECT sn,password
				FROM `students` 
				WHERE(`sn`="' . $_SESSION[studentUsername] . '");';
	
			if ($result = $this->registry->MySQL->query($sql)) {

				$row = $result->fetch_assoc();
				
				
				if (   is_array($row)
					&& $_SESSION[login_hash] == md5($row[sn] . $row[password])
				) {

					if (!isset($_SESSION[egenerateSession])) {  
						$_SESSION[egenerateSession] = true;  
						session_regenerate_id(true);  
					};
					
					return 1;
				};
				
			};

			
		};
		
		return 0;
	}
};

?>