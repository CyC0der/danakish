<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */


namespace engine;

final Class Registry
{
	private $vars = array();
	
	public function erLog($level,$line,$file,$no,$type,$message) {
	
		$levels = array(1,2,3,4);
		
		if(!in_array($level,$levels))
			return;
	
		$_SERVER['HTTP_USER_AGENT'] = $this->MySQL->real_escape_string($_SERVER['HTTP_USER_AGENT']);
		$_SERVER['HTTP_REFERER'] = $this->MySQL->real_escape_string($_SERVER['HTTP_REFERER']);
		$_SESSION['Users_ID'] = $this->MySQL->real_escape_string($_SESSION['Users_ID']);
		$_SERVER['QUERY_STRING']  = $this->MySQL->real_escape_string($_SERVER['QUERY_STRING']);
		
		$this->MySQL->query('INSERT INTO `prefix_error_log` SET
			`type`="'.$type.'",
			`message`="'.addslashes($message).'",
			`level`="'.$level.'",
			`time`= CURRENT_TIMESTAMP,
			`users_id`="'.addslashes($_SESSION['Users_ID']).'",
			`file`="'.str_replace('\\','/',$file).'",
			`line`="'.$line.'",
			`ip`="'.$_SERVER['SERVER_ADDR'].'",
			`referer`="'.$_SERVER['HTTP_REFERER'].'",
			`user_agent`="'.$_SERVER['HTTP_USER_AGENT'].'",
			`query`="'.$_SERVER['QUERY_STRING'].'",
			`no`="'.$no.'";
		');
	}

	public function __set($key, $value)
	{
		$this->vars[$key] = $value;
	}

	public function __get($key)
	{
		return $this->vars[$key];
	}
	
	public function get($key) {
		return (isset($this->vars[$key]) ? $this->vars[$key] : NULL);
	}

	public function set($key, $value) {
		$this->vars[$key] = $value;
	}
	
	public function has($key)
	{
    	return isset($this->vars[$key]);
  	}
};

?>