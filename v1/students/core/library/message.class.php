<?php
ob_start();


const XHHTP_REQIURED = 13;
const XHHTP_DB_FAULT = 14;
const XHHTP_INPUT_INVALID = 15;
const XHHTP_SUCCESS_EDIT = 30;
const XHHTP_SUCCESS_SAVED = 31;

function catchErrors() {
	
    if ($error = error_get_last()){
    switch($error['type']){
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
            echo json_encode(array('code'=>
				array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطای اجرا از سمت سرور <br>نوع: '.$error[type] . 
				"<br>فایل: ". $error[file] .
				"<br>پیام: " . $error[message] . 
				'<br>خط: ' .$error[line]
			)));
            break;
        }
    }
}

register_shutdown_function('catchErrors');

class message{
	
	private $codesList = array(13,14,15,30,31);
	private $msgList = array();
	
	function __construct() {
		return $this;
	}
	
	public function fatal($err){
		$this->add($err);
		ob_end_clean();
		echo json_encode(end($this->msgList));
		exit;
	}
	
	public function add($err){
		if(is_array($err)) {
			$this->msgList[] = $this->costum($err);
		} else if(is_numeric($err)) {
			$this->msgList[] = $err;
		} else {
			$this->msgList[] =	array('code'=>
				array(
				'status'=>0,
				'type'=>'danger',
				'text'=>$err
			));
		}
	}
	
	private function costum($err){
		return array('code'=>
			array(
			'status'=>$err[status],
			'type'=>$err[type],
			'text'=>$err[text]
		));
	}
	
	function showFirst(){
		$this->checkBufferNoice();
		ob_end_clean();
		echo json_encode($this->msgList[0]);
	}
	
	public function showLast(){
		$this->checkBufferNoice();
		ob_end_clean();
		echo json_encode(end($this->msgList));
	}
	
	private function checkBufferNoice(){
		$buf = ob_get_contents();
		if(!empty($buf)) {	
			$this->fatal(array(
				'status'=>0,
				'type'=> 'warning',
				'text'=> 'خطا! در پشت بافر مقادیر پیشبینی نشده وجود دارد'
			));
		}
	}
}


?>