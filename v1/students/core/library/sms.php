<?php

/********************************/
/* Code By Mr Korosh Raoufi 	*/
/* WwW.k2-4u.CoM            	*/
/********************************/


class SMS{

	private $username;
	private $password;
	private $sms_number;
	private $url = 'http://bakhtar-online.ir/API/SendSms.ashx?';

	function __construct($username,$password,$sms_number){
	
		$this->username =  $username;
		$this->password = $password;
		$this->sms_number = $sms_number;
	
	}
	
	public function SendStrError($num){
	
		switch($num){
			case 0: return "نام کاربری صحیح نمی‌باشد"; break;
			case 1: return "اعتبار شما برای ارسال کافی نیست"; break;
			case 2: return "اکانت شما دارای محدودیت ارسال می‌باشد"; break;
			case 3: return "نام کاربری پنل مشخص نشده است"; break;
			case 4: return "رمز عبور پنل مشخص نشده است"; break;
			case 5: return "شماره فرستنده مشخص نشده است"; break;
			case 6: return "شماره گیرنده مشخص نشده است"; break;
			case 7: return "متن ارسال مشخص نشده است"; break;
			case 8: return "پارامتر فلش معتبر نمی‌باشد"; break;
			case 9: return "شماره فرستنده معتبر نیست"; break;
			case 10: return "پیامک شما ارسال نشده ، سیستم موقتا قطع می‌باشد"; break;
			case 11: return "تعداد مخاطب ها از 80 نفر بیشتر است"; break;
			case 12: return "خطا نامشخص"; break;
		};
		
	}
	
	public function sendSMS($to,$msg){
		return file_get_contents($this->url.
				'username='.$this->username.
				'&password='.$this->password.
				'&from='.$this->sms_number.
				'&to='.$to.
				'&text='.urlencode($msg)
		);
	}
	
	
	public function sendSMSToList($list,$msg){
	
		$request = Array();
		
		$loop = ceil( count($list) / 80 );
		
		for($i=0;$i<$loop;$i++):

			$listStr = join( "," , array_slice($list,$i*80,80) );

			$request = array_merge ( $request , explode(',',file_get_contents($this->url.
				'username='.$this->username.
				'&password='.$this->password.
				'&from='.$this->sms_number.
				'&to='.$listStr.
				'&text='.urlencode($msg)
			)));
			
		endfor;
					
		return $request;
	}


	public function requestReader($request){
	
		$status[yes]=0;
		$status[no]=Array();
		
		foreach($request as $k=>$v):
		
			if(preg_match('/^([0-9]{10,50})$/i',$v)){
				$status[yes]++;
			}else{
				if($v >= 0 && $v <= 10){
					$status[no][$v]++;
				}else{
					$status[no][12]++;
				};
			};
		endforeach;
		
		return $status;
	}
	
	public function decodeToStrMsg($status){
	
		foreach($status[no] as $k=>$v){
			$out[] = $this->SendStrError($k)." ($v) ";
		};
		
		if($status[yes]){
			$out[] = "پیام با موفقیت ارسال شد (".$status[yes].")";
		}
		
		return $out;
	}
}

?>