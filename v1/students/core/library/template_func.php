<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */

namespace library;
 
class templateFunction
{
	/* include tpl file from view folder to the template  {inlude:tpl_file_name}  */
	protected function include_file($fileName)
	{
		$codes = file_get_contents(SYSTEM_PATH . '/' . SITE_ROOT .'/views/' . $fileName . '.' . $this->template_fileType);
		
		if ($codes == FALSE)
			throw new Exception('Template::includes() can\'t open file.');
		
		return $codes;
	}
	
	/* modirate and replace system url (for example mod_rewrite On or Off)  {url:index/logout?queryString} */
	protected function url($urlAddress)
	{
		return 'index.php?r='.$urlAddress;
	}
	
	/* replace text to the specific language {text:please inter username} */
	protected function text($text)
	{
	
	
	}
};

?>