// ParsleyConfig definition if not already set
window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n || {};

// Define then the messages
window.ParsleyConfig.i18n.fa = jQuery.extend(window.ParsleyConfig.i18n.fa || {}, {
  defaultMessage: "این مقدار صحیح نمی باشد",
  type: {
    email:        "این مقدار باید یک ایمیل معتبر باشد",
    url:          "این مقدار باید یک آدرس معتبر باشد",
    number:       "این مقدار باید یک عدد معتبر باشد",
    integer:      "این مقدار باید یک عدد صحیح معتبر باشد",
    digits:       "این مقدار باید یک عدد باشد",
    alphanum:     "این مقدار باید حروف الفبا باشد"
  },
  notblank:       "این مقدار نباید خالی باشد",
  required:       "این مقدار باید وارد شود",
  pattern:        "این مقدار به نظر می رسد نامعتبر است",
  min:            "این مقدیر باید بزرگتر با مساوی %s باشد",
  max:            "این مقدار باید کمتر و یا مساوی %s باشد",
  range:          "این مقدار باید بین %s و %s باشد",
  minlength:      "این مقدار بیش از حد کوتاه است. باید %s کاراکتر یا بیشتر باشد.",
  maxlength:      "این مقدار بیش از حد طولانی است. باید %s کاراکتر یا کمتر باشد.",
  length:         "این مقدار نامعتبر است و باید بین %s و %s باشد",
  mincheck:       "شما حداقل باید %s گزینه را انتخاب کنید.",
  maxcheck:       "شما حداکثر می‌توانید %s انتخاب داشته باشید.",
  check:          "باید بین %s و %s مورد انتخاب کنید",
  equalto:        "این مقدار باید یکسان باشد"
});

// If file is loaded after Parsley main file, auto-load locale
if ('undefined' !== typeof window.ParsleyValidator)
  window.ParsleyValidator.addCatalog('fa', window.ParsleyConfig.i18n.fa, true);


/* connect bootstrap to parsley (with has-feedback) */

Parsley.options = $.extend(Parsley.options,{
	trigger:'change',
	requiredMessage:'',
	successClass: "has-success has-feedback",
	errorClass: "has-error has-feedback",
	classHandler: function(el) {
		return el.$element.closest(".form-group");
	}
});

var parslyCalback = function(o,iconName) {
	o.parent().find('.parsley-sign').remove();
	o.not('[type="radio"],[type="checkbox"]').parent().
	append('<span class="parsley-sign glyphicon glyphicon-'+iconName+' form-control-feedback" aria-hidden="true"></span>');
}

window.Parsley.on('field:error', function() {
	parslyCalback(this.$element,'remove');
});

window.Parsley.on('field:success', function() {
	parslyCalback(this.$element,'ok');
});	

window.Parsley.addAsyncValidator('dublicate', function (xhr) {

	var ply = this.$element.parsley();
	
	window.ParsleyUI.removeError(ply,'dublicate');
	
	if(xhr.status == 200) {
		try{
			res = JSON.parse(xhr.responseText);
			
			if(typeof res != 'object') {
				console.log('server respoce is not JSON its: '+res);
				// error not show at this time (showing gritter is good idea)
				return true;
			}
				
			if(res.status != 1)
				window.ParsleyUI.addError(ply,'dublicate', res.message);

		} catch(err) {
			console.log('catch error!: '+err);
			// error not show at this time (showing gritter is good idea)
			return true;
		}
		
		return res.status;
	
	} else {
		console.log('http code is not right (connection faild maybe)  code:'+xhr.status);
		// error not show at this time (showing gritter is good idea)
		return true;
	}

}, 'php/remote.php');


var faChr = {
	fa:' آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ'.split(''),
	fa09:' آ۱۲۳۴۵۶۷۸۹۰پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ'.split(''),
	fa09en:'آ1234567890 ۱۲۳۴۵۶۷۸۹۰پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّگ'.split('')
}

window.Parsley.addValidator('persian', {
	 requirementType: 'string',
    validateString: function(value, req) {
		
		var chr = value.split('');
		var list = (typeof req !='undefined' ? faChr[req] : faChr['fa']);
		
		for(c in chr) {
			if($.inArray(chr[c],list) == -1)
				return false;
		}
    },
    messages: {
      fa: 'فارسی تایپ کنید',
    }
  });
