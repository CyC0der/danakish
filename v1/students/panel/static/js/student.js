

		$('[name="studentChangePassword"]').ft({
			validate: true,
			resetSuccess: true,
			bindValues: function(f) {   
				return {

					password: f.find('[name="password"]').val(),
					new_password: f.find('[name="new_password"]').val(),
					repeat_password: f.find('[name="repeat_password"]').val()
				};
			}
		});
		
		$(function() {
			$('.form-tooltip [type="text"]').tooltip({
				trigger: "focus"
			});
			$('.form-tooltip .form-control').tooltip({
				trigger: "focus"
			});
		})

	