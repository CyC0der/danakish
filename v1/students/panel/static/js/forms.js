var XHHTP_NO_CONNECTION = 10;
var XHHTP_JSON_BROKEN  = 11; 
var XHHTP_SERVER_500  = 12;  
var XHHTP_REQIURED = 13;
var XHHTP_DB_FAULT = 14;
var XHHTP_INPUT_INVALID = 15;
var XHHTP_SERVER_404  = 16;  
		
var XHHTP_SUCCESS_EDIT = 30;
var XHHTP_SUCCESS_SAVED = 31;


function gritter(c){	
	if(typeof c == 'undefined')
		return;
	
	if(typeof c.type == 'undefined') {
		
		var MSG = {
			10:{status:0,type:'danger',text:'ارتباط شما با اینترنت قطع است',title:'#10 خطا اتصال به اینترنت'},
			11:{status:0,type:'danger',text:'پاسخ سرویس دهنده نامشخص است، لطفا صفحه را دوباره بارگذاری کنید ، در صورت حل نشدن مشکل با پشتیبانی تماس بگیرید',title:'خطا از سمت سرویس دهنده #11'},
			12:{status:0,type:'danger',text:'سرویس دهنده درحال حاظر خارج از سرویس است ، لطفا بعداً مراجعه کنید',title:'خطا از سمت سرویس دهنده  #12'},
			13:{status:0,type:'danger',text:'لطفا فیلدهای قرمز را وارد کنید',title:'خطا ، اطلاعات ورودی کامل نیست'},
			14:{status:0,type:'danger',text:'سرویس دهنده دچار خطای بانک اطلاعاتی شده ، لطفا دقایق دیگر دوباره تلاش کنیدگ',title:'خطا از سمت سرویس دهنده #14'},
			15:{status:0,type:'danger',text:'مقادیر ورودی معتبر نیستند',title:'خطا از سمت سرویس دهنده  #15'},
			16:{status:0,type:'danger',text:'مقصد یافت نشد! ، لطفا به پشتیبانی اطلاع دهید',title:'خطا از سمت سرویس دهنده  #16'},
			30:{status:1,type:'success',text:'محتوای مورد نظر با موفقیت ویرایش شد',title:'ویرایش انجام شد'},
			31:{status:1,type:'success',text:'محتوای مورد نظر با موفقیت ثبت شد',title:'ثبت انجام شد'}
		};

		c = (typeof MSG[c] == 'undefined' ? {
			status:0,
			type:'danger',
			text:'سرویس دهنده با خطا مواجه شده ، کد خطا ('+c+')',
			title:'خطا از سمت سرویس دهنده'
		} : MSG[c]);
	}
	
	$.gritter.add({
		title: c.title,
		text: c.text,
		class_name: c.type
	});
	
	return c.status;
}

function loading(container) {
	
	var loadingHTML = '<div class="form-loading">'+
	'	<div class="back"></div>'+
	'	<div class="msg">'+
	'		<img src="panel/static/img/load1.gif"><br>'+
	'		لطفا صبر کنید ...'+
	'	</div>'+
	'</div>';
	
	container.append(loadingHTML);
	container.css('position','relative');
	
	this.element = container.find('.form-loading');
	
	this.show = function() {
		this.element.show();
	}
	
	this.hide = function() {
		this.element.hide();
	}
	
}

(function ( $ ) {
	

	function ftPlugin(form,opt){
		
		this.form = form;
		this.xhttpOpt = {};
		this.btn = form.find('[type="submit"]');
		
		if(typeof opt == 'undefined')
				var opt = {};
			
		this.resetSuccess = opt.resetSuccess;
		this.closeModal = opt.closeModal;
		this.id = opt.id;
		
		this.setupAjax = function(opt){	
			this.xhttpOpt = opt;
			return this;
		}
		
		this.bindValues = function(cb) {
			this.getValues = cb;
			return this;
		}
		
		this.validate = function(opt,customCBack) {
		
			this.parsley = this.form.parsley(opt);
			
			if(typeof customCBack != 'undefined')
				customCBack(this.parsley );
			return this;
		}

		this.setValues = function(v) {
			for(i in v) {
				var e = this.form[0][i];
				if(typeof e!='undefined')
				switch(e.type) {
					case 'checkbox':
						$(e).prop('checked',(v[i]?true:false));
					break;
					case 'radio':
						$(e).find('[value="'+v[i]+'"]').prop('checked',(v[i]?true:false));
					break;
					default:
						$(e).val(v[i]);
				}
			}
		}
			
		this.xhttp = function() {
		
			var $this = this;
			
			if(typeof this.getValues != 'undefined') {
				this.xhttpOpt['data'] = this.getValues(this.form);
				console.log(this.xhttpOpt['data']);
			}
			
			var url = "?r=index/forms&name=" + (typeof this.form[0].name != 'undefined' ? this.form[0].name : '')
			+ "&id=" + (typeof this.id != 'undefined' ? this.id : '');

			 $.ajax(
				 $.extend({		
				   url:url,
				   type:"POST",
				   dataType:'text'
				}, this.xhttpOpt)
				
			).done(function(d){


				if(/^([0-9]{2})$/i.test($.trim(d))) {
					var res = gritter(d);
				} else {
					try {
						d = JSON.parse(d);
						if(typeof d != 'object')
							throw "server response is not JSON Object! (its number may)";

						var res = gritter(d.code);
						
						if(res) {
							if(typeof d.location != 'undefined')
								window.location.href = d.location;
						}


					} catch(err) {
						var res = gritter(XHHTP_JSON_BROKEN);
					};
				};

				if($this.resetSuccess && res)
					$this.reset();
				
				if($this.closeModal && res &&
					typeof $('#win-'+$this.form[0].name) != 'undefined'
				) {
					$('#win-'+$this.form[0].name).modal('hide');
				}
				
				if(typeof $this.btn!='undefined') {
					$this.btn.button('reset');
					$this.loading.hide();
				}
						
				if(typeof $this.done != 'undefined')
					$this.done(d,res);
				
			}).fail(function(xhr) {
				
				console.log($this.xhttpOpt['data']);
				
				switch(xhr.status) {
					case 500: gritter(XHHTP_SERVER_500); break;
					case 404: gritter(XHHTP_SERVER_404); break;
					case 0:
					default:
						gritter(XHHTP_NO_CONNECTION); 
					break;
				}
				
				console.log('HTTP Status Code: ' + xhr.status);
				
				if(typeof $this.btn!='undefined') {
					$this.btn.button('reset');
					$this.loading.hide();
				}
			});
		};
	
		this.reset = function() {
			this.form[0].reset();
			this.form.find('.parsley-sign').remove();
			this.form.find('.form-group').removeClass('has-success').removeClass('has-feedback');
		};
		
		this.fetchData = function(url) {
			
			var $this = this;
			
			this.loading.show();
			
			try {
				$.getJSON(url, function(data,stauts){
					//("success", "notmodified", "error", "timeout", or "parsererror").
					// if its a int
					
					if(stauts=='success') {
						$this.setValues(data);
						$this.loading.hide();

					}
					
				}).fail(function() {
					$this.loading.hide();
				});
			} catch(err) {
				$this.loading.hide();
			};
		}

		this.loading = new loading(this.form);
		
		if(typeof opt.done != 'undefined')
			this.done = opt.done;
			
		if(typeof opt.setupAjax != 'undefined')
			this.setupAjax(opt.setupAjax);
		
		if(typeof opt.bindValues != 'undefined')
			this.bindValues(opt.bindValues);
		
		if(typeof opt.validate != 'undefined')
			this.validate(opt.validate.opt,opt.validate.customCBack);
		
		if(typeof opt.setValues != 'undefined')
			this.setValues(opt.setValues)
		
		if(typeof this.btn != 'undefined') {
			
			this.btn.data('loading-text','لطفا صبر کنید ...');

			var $this = this;
			
			this.btn.click(function() {
				
				if(typeof $this.parsley != 'undefined')
					if(!$this.parsley.validate()) 
						return false;

				$this.btn.button('loading');
				$this.loading.show();
				
				if(typeof $this.getValues != 'undefined')
					$this.xhttp();

				return false;
			});
		};
	
		return this;
		
	}
	
	 $.fn.ft = function(opt) {
		 return new ftPlugin(this,opt);
	 }
	
}( jQuery ));


var areaCodes = ['',41,44,45,31,26,84,77,21,38,56,51,58,61,24,23,54,71,28,25,78,34,083,74,17,13,66,11,86,76,81,35];

if($('[name="tell"]').size()!=0) {
	
	$('[name="tell"]').css({
		'padding-right':'38px'
	}).parent().css('position','relative').append(
		'<span style="position:absolute;top:31px;right:12px" class="city-areacode"></span>'
	);
	
	$('[name="city_id"]').change(function() {
		$('.city-areacode').text('0'+areaCodes[$(this).val()]+'-');
	});
}

if($('[name="town_id"]').size()!=0)
$('[name="city_id"]').change(function() {
	if($(this).val() == "") {
		$('[name="town_id"]').addClass('disabled').prop('disabled',true);
	} else {
		$('[name="town_id"]').removeClass('disabled').prop('disabled',false);
		$('[name="town_id"] option').hide().filter('[data-city-id="'+$(this).val()+'"],[data-city-id=""]').show();
		$('[name="town_id"] option:eq(0)').prop('selected',true);
	}

});
