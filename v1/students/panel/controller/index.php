<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */
 
 namespace controller;
 
Class index Extends \engine\Controller
{
	public function index() {
		
		$this->template->load('index');

		list($firstname,$lastname,$level,$class,$sn,$exams_id,$name) = $this->MySQL->query('SELECT 
			firstname,lastname,students.level as level,class ,sn,exams_id,name
			FROM `students` INNER JOIN exams
			ON students.level = exams.level AND
			students.school_id = exams.school_id
			WHERE(exams.school_id=1 AND sn="'.$_SESSION[studentUserID].'");'
		)->fetch_array();
		

		$examInfo = $this->MySQL->query('SELECT 
			`answer`, `olom_avg`, `motaleat_avg`, `zaban_avg`, `riazi_avg`, `farsi_avg`, `arabi_avg`, `qoran_avg`
			FROM `exams` 
			WHERE(`exams_id`="'.$exams_id.'");'
		)->fetch_assoc();
		
		$points = $this->MySQL->query('SELECT 
			code,answer,scores,
			`olom_taraz`, `motaleat_taraz`, `zaban_taraz`, 
			`riazi_taraz`, `farsi_taraz`, `arabi_taraz`, `qoran_taraz`
			FROM 
			`points` 
			WHERE(
				`exams_id`="'.$exams_id.'" AND 
				sn="'.$_SESSION[studentUserID].'");'
		)->fetch_assoc();
		
		
		$rank_level = $this->MySQL->query('SELECT  
			FIND_IN_SET(`olom_taraz`, 
				(SELECT GROUP_CONCAT( `olom_taraz` ORDER BY `olom_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
			) AS olom_rank_level,
			FIND_IN_SET(`motaleat_taraz`, 
				(SELECT GROUP_CONCAT( `motaleat_taraz` ORDER BY `motaleat_taraz` DESC ) FROM `points`
				WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
			) AS motaleat_rank_level,
			FIND_IN_SET(`zaban_taraz`,
				(SELECT GROUP_CONCAT( `zaban_taraz` ORDER BY `zaban_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
			) AS zaban_rank_level,
			FIND_IN_SET(`riazi_taraz`, 
				(SELECT GROUP_CONCAT( `riazi_taraz` ORDER BY `riazi_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
			) AS riazi_rank_level,
			FIND_IN_SET(`farsi_taraz`, 
				(SELECT GROUP_CONCAT( `farsi_taraz` ORDER BY `farsi_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
			) AS farsi_rank_level,
			FIND_IN_SET(`arabi_taraz`, 
				(SELECT GROUP_CONCAT( `arabi_taraz` ORDER BY `arabi_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
			) AS arabi_rank_level,
			FIND_IN_SET(`qoran_taraz`, 
				(SELECT GROUP_CONCAT( `qoran_taraz` ORDER BY `qoran_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
			) AS qoran_rank_level
			FROM `points`
			WHERE(sn =  "'.$_SESSION[studentUserID].'");'
		)->fetch_assoc();

		
		$rank_class = $this->MySQL->query('SELECT  
			FIND_IN_SET(`olom_taraz`, 
				(SELECT GROUP_CONCAT( `olom_taraz` ORDER BY `olom_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
			) AS olom_rank_class,
			FIND_IN_SET(`motaleat_taraz`, 
				(SELECT GROUP_CONCAT( `motaleat_taraz` ORDER BY `motaleat_taraz` DESC ) FROM `points`
				WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
			) AS motaleat_rank_class,
			FIND_IN_SET(`zaban_taraz`,
				(SELECT GROUP_CONCAT( `zaban_taraz` ORDER BY `zaban_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
			) AS zaban_rank_class,
			FIND_IN_SET(`riazi_taraz`, 
				(SELECT GROUP_CONCAT( `riazi_taraz` ORDER BY `riazi_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
			) AS riazi_rank_class,
			FIND_IN_SET(`farsi_taraz`, 
				(SELECT GROUP_CONCAT( `farsi_taraz` ORDER BY `farsi_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
			) AS farsi_rank_class,
			FIND_IN_SET(`arabi_taraz`, 
				(SELECT GROUP_CONCAT( `arabi_taraz` ORDER BY `arabi_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
			) AS arabi_rank_class,
			FIND_IN_SET(`qoran_taraz`, 
				(SELECT GROUP_CONCAT( `qoran_taraz` ORDER BY `qoran_taraz` DESC ) FROM `points` 
				WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
			) AS qoran_rank_class
			FROM `points`
			WHERE(sn =  "'.$_SESSION[studentUserID].'");'
		)->fetch_assoc();
		
		/*
		SELECT avg(olom) as olom_5avg from (SELECT olom FROM `points` WHERE(level=7)  ORDER BY `olom_taraz` DESC LIMIT 0,5) a

SELECT
			(SELECT ROUND(AVG(olom),2)FROM (SELECT olom FROM `points` WHERE(level=7)  ORDER BY `olom_taraz` DESC LIMIT 0,5) a1 ) as olom_5avg 

	*/
		
		$avg5 = $this->MySQL->query('SELECT
		
			(SELECT ROUND(AVG(olom),2) 
				FROM (SELECT olom FROM `points` WHERE(level='.$level.')  ORDER BY `olom_taraz` DESC LIMIT 0,5) a1
			) as olom_5avg ,
			
			(SELECT ROUND(AVG(motaleat),2) 
				FROM (SELECT motaleat FROM `points` WHERE(level='.$level.')  ORDER BY `motaleat_taraz` DESC LIMIT 0,5) a2
			) as motaleat_5avg ,
			
			(SELECT ROUND(AVG(zaban),2) 
				FROM (SELECT zaban FROM `points` WHERE(level='.$level.')  ORDER BY `zaban_taraz` DESC LIMIT 0,5) a2
			) as zaban_5avg ,
			
			(SELECT ROUND(AVG(riazi),2) 
				FROM (SELECT riazi FROM `points` WHERE(level='.$level.')  ORDER BY `riazi_taraz` DESC LIMIT 0,5) a3
			) as riazi_5avg ,
			
			(SELECT ROUND(AVG(farsi),2) 
				FROM (SELECT farsi FROM `points` WHERE(level='.$level.')  ORDER BY `farsi_taraz` DESC LIMIT 0,5) a3
			) as farsi_5avg ,
			
			(SELECT ROUND(AVG(arabi),2) 
				FROM (SELECT arabi FROM `points` WHERE(level='.$level.')  ORDER BY `arabi_taraz` DESC LIMIT 0,5) a3
			) as arabi_5avg ,
			
			(SELECT ROUND(AVG(qoran),2) 
				FROM (SELECT qoran FROM `points` WHERE(level='.$level.')  ORDER BY `qoran_taraz` DESC LIMIT 0,5) a3
			) as qoran_5avg;'
			
		)->fetch_assoc();
		
		$examAnswer = json_decode($examInfo['answer'], true);	
		$scores = json_decode($points['scores'], true);
		$code = json_decode($points['code'], true);

		$this->template->assign(array(
			'olom_status' => $this->calcStatus($points['olom_taraz']),
			'motaleat_status'  => $this->calcStatus($points['motaleat_taraz']), 
			'zaban_status'  => $this->calcStatus($points['zaban_taraz']),
			'riazi_status'  => $this->calcStatus($points['riazi_taraz']), 
			'farsi_status'  => $this->calcStatus($points['farsi_taraz']),
			'arabi_status'  => $this->calcStatus($points['arabi_taraz']),
			'qoran_status'  => $this->calcStatus($points['qoran_taraz'])
		));
		
		
		$all_avg = round((($points['olom_taraz']*2) +
		($points['motaleat_taraz']*2) + 
		($points['zaban_taraz']) + 
		($points['riazi_taraz']*3) + 
		($points['farsi_taraz']*2) + 
		($points['arabi_taraz']) + 
		($points['qoran_taraz']*2)) / 13);
			
		$this->template->assign('all_avg',$all_avg);
		$this->template->assign('all_status',$this->calcStatus($all_avg));
		$this->template->assign('level',$level);// . ' ' . $exams_id.' '.$name);
		$this->template->assign('class',$class);
		$this->template->assign('sn',$sn);
		$this->template->assign($this->getData('count'));
		$this->template->assign($points);
		$this->template->assign($examInfo);
		$this->template->assign($rank_level);
		$this->template->assign($rank_class);
		$this->template->assign($avg5);
		$this->template->assign($scores['true']);
		$this->template->assign($scores['false']);
		$this->template->assign($scores['percent']);
		$this->template->assign($scores['empty']);	

		$picPath = 'panel/static/user_pics/'.$_SESSION[studentUserID].'.jpg';
		$this->template->assign(array(
			'static_path' => 'panel/static',
			'student_pic'=> (file_exists($picPath)?$picPath:'panel/static/default.jpg') ,
			'student_name' => $firstname . ' ' .$lastname
		));

		
		for($i=1;$i<=20;$i++){
			
			$a=array('count'=>$i);

			if($code[$i] != '') {
				$a[$code[$i]] = 'fill';
			}
			
			if($examAnswer[$i] != '') {
				$a[$examAnswer[$i]] .= ' answer glyphicon glyphicon-ok';
			}
			
			if($code[$i] == '' && $examAnswer[$i] != '') {
				$a['A'] = $a['B'] = $a['C'] = $a['D'] = 'empty';
				$a[$examAnswer[$i]] = 'empty answer glyphicon glyphicon-ok';
			}

			$this->template->add_block('answer1',$a);
		}
			
		for($i=21;$i<=40;$i++){
			
			$a=array('count'=>$i);

			if($code[$i] != '') {
				$a[$code[$i]] = 'fill';
			}
			
			if($examAnswer[$i] != '') {
				$a[$examAnswer[$i]] .= ' answer glyphicon glyphicon-ok';
			}
			
			if($code[$i] == '' && $examAnswer[$i] != '') {
				$a['A'] = $a['B'] = $a['C'] = $a['D'] = 'empty';
				$a[$examAnswer[$i]] = 'empty answer glyphicon glyphicon-ok';
			}
			
			$this->template->add_block('answer2',$a);
		}
			
		for($i=41;$i<=60;$i++){
			
			$a=array('count'=>$i);

			if($code[$i] != '') {
				$a[$code[$i]] = 'fill';
			}
			
			if($examAnswer[$i] != '') {
				$a[$examAnswer[$i]] .= ' answer glyphicon glyphicon-ok';
			}
			
			if($code[$i] == '' && $examAnswer[$i] != '') {
				$a['A'] = $a['B'] = $a['C'] = $a['D'] = 'empty';
				$a[$examAnswer[$i]] = 'empty answer glyphicon glyphicon-ok';
			}
			
			$this->template->add_block('answer3',$a);
		}
		
		for($i=61;$i<=80;$i++){
			
			$a=array('count'=>$i);
			
			if($code[$i] != '') {
				$a[$code[$i]] = 'fill';
			}
			
			if($examAnswer[$i] != '') {
				$a[$examAnswer[$i]] .= ' answer glyphicon glyphicon-ok';
			}
			
			if($code[$i] == '' && $examAnswer[$i] != '') {
				$a[$code[$i]] = 'empty';
			}
			
			$this->template->add_block('answer4',$a);
		}
		
		for($i=81;$i<=100;$i++){
			
			$a=array('count'=>$i);
			
			if($code[$i] != '') {
				$a[$code[$i]] = 'fill';
			}
			
			if($examAnswer[$i] != '') {
				$a[$examAnswer[$i]] .= ' answer glyphicon glyphicon-ok';
			}
			
			if($code[$i] == '' && $examAnswer[$i] != '') {
				$a[$code[$i]] = 'empty';
			}
			
			$this->template->add_block('answer5',$a);
		}
		
		for($i=101;$i<=120;$i++){
			
			$a=array('count'=>$i);
			
			if($code[$i] != '') {
				$a[$code[$i]] = 'fill';
			}
			
			if($examAnswer[$i] != '') {
				$a[$examAnswer[$i]] .= ' answer glyphicon glyphicon-ok';
			}

			
			$this->template->add_block('answer6',$a);
		}
		$this->template->exec();
	}
	
	
	public function forms() {

		switch($_GET[name]) {
			
			case 'studentChangePassword':
				$this->studentChangePassword();
			break;

			default:
				echo 'No FORM selected!';
		}
		
	}
	
	private function studentChangePassword() {

		require_once('core/library/message.class.php');
		require_once('core/library/validation.class.php');
		
		$message = new \message();
		
		$v = new \validation(3,$MySQL,$message);

		$v->post('password')->required();
		$v->post('new_password')->required();
		$v->post('repeat_password')->required();

		if($v->check()){
			
			$p = $v->values();
			
			$password = $this->MySQL->query('SELECT `password` FROM `students` WHERE(sn="'.$_SESSION[studentUserID].'");')->fetch_assoc();

			if(!$password || $password[password] != $p[password]) {
				$v->message->fatal('رمز عبور وارد شده اشتباه است');
			}
			
			if($p[repeat_password] != $p[new_password]) {
				$v->message->fatal('رمز عبور جدید با تکرار آن برابر نیست');
			}

			$result = $this->MySQL->query('UPDATE `students` SET 
				`password`="'.$this->MySQL->real_escape_string($p[new_password]).'"
				WHERE(sn="'.$_SESSION[studentUserID].'");'
			);

			if(!$result)
				$v->message->fatal('خطا از سمت دیتابیست :' . $this->MySQL->error);
			
			$v->message->add(XHHTP_SUCCESS_EDIT);
		}

		$v->message->showFirst();
		
	}
	
	private function getData($a){
		return array(
			'olom_count'=>20,
			'motaleat_count'=>10,
			'zaban_count'=>7,
			'riazi_count'=>25,
			'farsi_count'=>10,
			'arabi_count'=>8,
			'qoran_count'=>10
		);
	}
	
	private function calcStatus($n){
		if($n >= 6000) {
			return '<div class="label label-primary">عالی</div>';
		} else if($n > 5300 && $n < 6000) {
			return '<div class="label label-success">بالای متوسط</div>';
		} else if($n > 5000 && $n <= 5300) {
			return '<div class="label label-success">متوسط</div>';
		} else if($n > 4200 && $n < 5000) {
			return '<div class="label label-warning">زیر متوسط</div>';
		} else if($n < 4200) {
			return '<div class="label label-danger">نیازمند تلاش</div>';
		}
	}
	
	public function logout() {
		
		session_destroy();
		header('location: index.php?r=login');
	}
	
};

?>
