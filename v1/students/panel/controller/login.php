<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */
 
 namespace controller;
 
Class login Extends \engine\Controller
{
	public function index()
	{
		$this->template->load('login');
		
		if(isset($_POST['login'])){

			if(preg_match('/^([0-9]{8,10})$/i',$_POST['username'])) {

				if (!empty($_POST['password'])) {
				
					$result = $this->MySQL->query('SELECT 
							sn,password FROM 
							`students` 
							WHERE(`sn`="'.($_POST['username']).'");'
					);
					
					$row = $result->fetch_assoc();

					if(!$row){
						$msg='نام کاربري يا رمزعبور را اشتباه وارد کرده ايد';
					}else{
						if($row['sn']==$_POST['username'] && $row['password']==$_POST['password'])
						{
							$_SESSION[studentUsername] = $row['sn'];
							$_SESSION[studentUserID]=$row['sn'];
							$_SESSION['login_hash']=md5($row['sn'].$row['password']);
							header('location:index.php');
							
						}else{
							$msg='نام کاربري يا رمزعبور را اشتباه وارد کرده ايد';
						};
					};
				}else{
					$msg='لطفا پسورد را وارد کنيد';
				};
			}else{
				$msg='لطفا نام کاربري را درست وارد کنيد';
			}
		};

		$this->template->assign('message' ,$msg);
		
		
		$this->template->assign('static_path','panel/static');
		
		$this->template->exec();
	}
	
	private function calcStatus($n){
		if($n >= 6000) {
			return 'عالی';
		} else if($n > 5300 && $n < 6000) {
			return 'بالای متوسط';
		} else if($n > 5000 && $n <= 5300) {
			return 'متوسط';
		} else if($n > 4200 && $n < 5000) {
			return 'زیر متوسط';
		} else if($n < 4200) {
			return 'نیازمند تلاش';
		}
	}
	
	public function sms()
	{
		
		require_once ('core/library/sms.php');
		
		$this->userSMS = new \SMS(SMS_USERNAME,SMS_PASWORD,SMS_NUMBER);
		
		$_GET[text] = preg_replace('/\D/', '', 
			strtr($_GET[text],
				array('١'=>'1','٢'=>'2','٣'=>'3','٤'=>'4','٥'=>'5','٦'=>'6','٧'=>'7','٨'=>'8','٩'=>'9','٠'=>'0')
		));
		
		if(!preg_match('/^([0-9]{10,15})$/i',$_GET[cell])) {
			echo'شماره ارسال کننده نامعتبر است';
			exit;
		}
		
		if(!preg_match('/^([0-9]{7,10})$/i',$_GET[text])) {
			$this->userSMS->sendSMS($_GET[cell],'کد ارسال شده اشتباه است');
			exit;
		}
		

		$len = strlen($_GET[text]);
		
		if($len == 7) {

			$this->getResultByPassowrd();
			
		} else if($len == 10) {
			
			$this->getParentCell();
			
		} else {

			$this->userSMS->sendSMS($_GET[cell],'کد ارسال شده اشتباه است');
			
		}
		
		
		
	}
	
	private function getResultByPassowrd() {
		

		$taraz = $this->MySQL->query('SELECT  
			lastname,`olom_taraz`, `motaleat_taraz`, `zaban_taraz`, `riazi_taraz`, `farsi_taraz`, `arabi_taraz`, `qoran_taraz` 
			FROM `points` 
			INNER JOIN `students`
			USING(sn)
			WHERE(password="'.$this->MySQL->real_escape_string($_GET['text']).'");
		')->fetch_assoc();
		
		if($taraz) {
			 $message = $taraz['lastname']."\r\n\r\n".
			'علوم:'.$this->calcStatus($taraz['olom_taraz'])."\r\n".
			'اجتماعی:'.$this->calcStatus($taraz['motaleat_taraz'])."\r\n".
			'زبان:'.$this->calcStatus($taraz['zaban_taraz'])."\r\n".
			'ریاضی:'.$this->calcStatus($taraz['riazi_taraz'])."\r\n".
			'فارسی:'.$this->calcStatus($taraz['farsi_taraz'])."\r\n".
			'عربی:'.$this->calcStatus($taraz['arabi_taraz'])."\r\n".
			'دینی:'.$this->calcStatus($taraz['qoran_taraz']);
			
			$this->userSMS->sendSMS($_GET[cell],$message);
		} else {
			$this->userSMS->sendSMS($_GET[cell],'کد وارد شده اشتباه است');
		}
	}
	
	private function getParentCell(){
		

		$res =  $this->MySQL->query('SELECT 
			sn,parent_cell,firstname,lastname 
			FROM students 
			WHERE(sn="'.$_GET[text].'" OR parent_cell="'.$_GET[cell].'");
		')->fetch_assoc();
		
		if(!empty($res[parent_cell])) {
			$this->userSMS->sendSMS($_GET[cell],' شماره شما قبلا به اسم ' . $res[firstname] . ' ' . $res[lastname]. ' ثبت شده است');
			exit;
		}
		
		if(!$res[sn]) {
			$this->userSMS->sendSMS($_GET[cell],' دانش آموزی با کد ملی ارسال شده در سیستم وجود ندارد ');
			exit;
		}
		
		$qy =  $this->MySQL->query('UPDATE students SET parent_cell="'.$_GET[cell].'" WHERE(sn="'.$_GET[text].'");');
		if($qy) {
			$this->userSMS->sendSMS($_GET[cell],' شماره شما برای دانش آموز ' . $res[firstname] . ' ' . $res[lastname]. ' با موفقیت ثبت شد');
		} else {
			$this->userSMS->sendSMS($_GET[cell],' سیستم با مشکل مواجه شده');
		}
	}
};

?>
