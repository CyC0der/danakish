<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */
 
 namespace controller;
 
Class graph Extends \engine\Controller
{
	public function index()
	{
		$PacksID = 1;
		
		header("Content-type: image/jpeg");

		require_once ('core/library/jpgraph/jpgraph.php');
		require_once ('core/library/jpgraph/jpgraph_bar.php');
		require_once ('core/library/jpgraph/jpgraph_line.php');
		require_once('core/library/gdfarsi/FarsiGD.php');

		$gd = new \FarsiGD();
		
		$p = $this->MySQL->query('SELECT 
			`session1_first`, `session1_second`, `session1_task`, 
			`session2_first`, `session2_second`, `session2_task`, 
			`session3_first`, `session3_second`, `session3_task`, 
			`session4_first`, `session4_second`, `session4_task`, 
			`session5_first`, `session5_second`, `session5_task`, 
			`session6_first`, `session6_second`, `session6_task`, 
			`session7_first`, `session7_second`, `session7_task`, 
			`session8_first`, `session8_second`, `session8_task`, 
			`session9_first`, `session9_second`, `session9_task`, 
			`session10_first`, `session10_second`, `session10_task`,
			`current_session`+0
			FROM `premium_packs_students` 
			INNER JOIN `premium_packs`
			 USING(`premium_packs_id`)
			WHERE(`premium_packs_id`="'.$PacksID.'" AND `students_id`="'.$_SESSION[studentUserID].'");'
		)->fetch_array();
		

		$current_session = $p[30];
		

		//bar1
		$data1y=array($p[0],$p[3],$p[6],$p[9],$p[12],$p[15],$p[18],$p[21],$p[24],$p[27]);
		//bar2
		$data2y=array($p[1],$p[4],$p[7],$p[10],$p[13],$p[16],$p[19],$p[22],$p[25],$p[28]);
		//bar3
		$data3y=array($p[2],$p[5],$p[8],$p[11],$p[14],$p[17],$p[20],$p[23],$p[26],$p[29]);

		$graph = new \Graph(750,320,'auto');
		$graph->SetScale("textlin");

		$graph->SetMargin(25,2,5,2);


		$graph->yaxis->HideTicks(false,false);

		$labels = array(
			$gd->persianText('جلسه اول', 'fa', 'normal'),
			$gd->persianText('جلسه دوم', 'fa', 'normal'),
			$gd->persianText('جلسه سوم', 'fa', 'normal'),
			$gd->persianText('جلسه چهارم', 'fa', 'normal'),
			$gd->persianText('جلسه پنجم', 'fa', 'normal'),
			$gd->persianText('جلسه ششم', 'fa', 'normal'),
			$gd->persianText('جلسه هفتم', 'fa', 'normal'),
			$gd->persianText('جلسه هشتم', 'fa', 'normal'),
			$gd->persianText('جلسه نهم', 'fa', 'normal'),
			$gd->persianText('جلسه دهم', 'fa', 'normal')
		);
				

		$graph->xaxis->SetTickLabels($labels);
		$graph->xaxis->SetFont(FF_KODAK,FS_NORMAL,12);

		// Create the bar plots
		$b1plot = new \BarPlot($data1y);
		$b2plot = new \BarPlot($data2y);
		$b3plot = new \BarPlot($data3y);

		// Create the grouped bar plot
		$gbplot = new \GroupBarPlot(array($b1plot,$b2plot,$b3plot));


		// ...and add it to the graPH


		$graph->Add($gbplot);

		$b1plot->SetShadow("#a70062",2,2); 
		$b1plot->SetColor("#a70062");
		$b1plot->SetFillColor("#ed008c");
		$b1plot->SetLegend($gd->persianText('آزمون اولیه', 'fa', 'normal'));

		$b2plot->SetShadow("#225a91",2,2);
		$b2plot->SetColor("#225a91");
		$b2plot->SetFillColor("#3587d9");
		$b2plot->SetLegend($gd->persianText('آزمون تکمیلی', 'fa', 'normal'));

		$b3plot->SetShadow("#5c7615",2,2); 
		$b3plot->SetColor("#5c7615");
		$b3plot->SetFillColor("#94c11c");
		$b3plot->SetLegend($gd->persianText('تکلیف', 'fa', 'normal'));


		$graph->legend->SetFont(FF_KODAK,FS_NORMAL,12);
		$graph->legend->SetFrameWeight(1);
		$graph->legend->SetColumns(3);
		$graph->legend->SetColor('#4E4E4E','#f6f6f6');


	
		$band = new \PlotBand(VERTICAL,BAND_RDIAG,$current_session-1,$current_session,'khaki4');
		$band->SetDensity(90);
		$band->ShowFrame(true);
		$band->SetOrder(DEPTH_FRONT);
		$graph->Add($band);


		// Display the graph
		$graph->Stroke();
	}
};

?>
