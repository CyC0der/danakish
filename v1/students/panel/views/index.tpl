﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=730, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>پنل مدیریت دانش آموز</title>
    <link href="{static_path}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{static_path}/lib/font-awesome/css/font-awesome.min.css">
    <!--if lt IE 9script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')-->
    <link rel="stylesheet" type="text/css" href="{static_path}/lib/jquery.gritter/jquery.gritter.css">
    <link href="{static_path}/css/style.css" rel="stylesheet">
    <style>
	
        
.profile-avatar {
	width: 150px;
}

.answer-table-info{
	width:20px;
	height:15px;
	font-size:9px;
	padding:2px 0;
	text-align:center;
	color:#000;
	border:1px solid #000;
	
}	 
    
.answer-table{
	float:right;
	border-collapse:separate;
	border-spacing:2px;
	width:16%;
	min-width:90px;
}

.answer-table td:not(.count){
	width:11%;
	height:15px;
	border:1px solid;
	padding:0;
}

.count{
	width:11%;
	height:15px;
	color:#000;
	font-size:9px;
	line-height:9px;
	padding:0;
}

.answer-table td{
	font-size:9px;
	padding:0;
    display: table-cell;
	text-align:center;
	color:#000;
}

.fill{
	background-color:red;
	color:#FFF !important;
}

.answer{
	color:green !important;
}

.table-points .label{
	display:block;
	width:60px;
	white-space: normal;
	line-height:13px;
}

.table-points td,.table-points th{
	font-size:9pt;
	text-align:center;
	vertical-align: middle !important;
	height:46px;
}

.table-points th{
	text-align:center;
	font-size:8pt;
}

.all_avg{
	font-size:26px;
}

.stu-info b{
	 font-weight:bold !important;
}
.stu-info .label{
	display:inline;
}

.fill.answer{
	background-color:green;
	color:#FFF !important;
}

.empty{
	border:1px solid #ff7200 !important;
	background-color:#fffca8;
}


</style>
</head>
<body>
    <!-- Navbar - Start -->
    <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-left">
                <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle">
                    <span class="fa fa-gear"></span>
                </button>
                <a href="#" class="navbar-brand"><span>آزمون های جامع داناکیش</span></a>
            </div>
            <div class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right user-nav">
                    <li class="dropdown profile_menu">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <b class="caret"></b>
                            <span>{student_name}</span>
                            <img alt="Avatar" style="width:25px" src="{student_pic}">
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="?r=index/logout">خروج</a></li>
                        </ul>
                    </li>
                </ul>


            </div>
        </div>
    </div>
    <!-- Navbar - End -->

    <!-- ourBody START -->
    <div id="cl-wrapper">

        <div id="pcont" class="container-fluid">
            <!--NedaArea START-->
            <div class="cl-mcont">

                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="tab-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tabInfo">اطللاعات</a></li>
                                <li><a data-toggle="tab" href="#tabStudentChangePassword">تغییر رمزعبور</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tabInfo" class="tab-pane active cont">
                                    <div class="avatar-upload pull-left">
                                        <img src="{student_pic}" class=" profile-avatar img-thumbnail">

                                    </div>
								
<span class="stu-info">
<p><b>نمره تراز کل:</b> <span class="badge all_avg">{all_avg}</span> </p>
<p><b>سطح علمی کل:</b> {all_status} </p>
<hr>	  
<p><b>نام:</b> {student_name} </p>
<p><b>کد دانش آموزی:</b> {sn} </p>
<p><b>مدرسه:</b> دانا </p>
<p><b>پایه:</b> {level} </p>
<p><b>کلاس:</b> {class} </p>					
</span>

                                    <div class="clearfix"></div>
                                </div>
								<div id="tabStudentChangePassword" class="tab-pane">
                                    <form role="form" name="studentChangePassword">
                                        <div class="form-group">
                                            <label for="password"> رمز عبور فعلی :</label>
                                            <input type="password" class="form-control" name="password" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password"> رمز عبور جدید :</label>
                                            <input class="form-control" type="password" name="new_password" id="pass" data-parsley-type="integer" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="repeat_password"> رمز عبور جدید :</label>
                                            <input class="form-control" type="password" name="repeat_password" data-parsley-equalto="#pass" required>
                                        </div>
                                        <button type="submit" class="btn btn-primary">تغییر رمزعبور</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-8  col-sm-12 side-left">

                        <div class="block-flat">
                            <div class="header">
                                <h4><i class="glyphicon glyphicon-stats"></i> نمرات</h4>
                            </div>
                            <div class="content table-responsive">
                               <table class="table table-striped table-bordered table-points">
	<thead>
		<tr>
			<th rowspan="2">نام درس</th>
			<th rowspan="2">تعداد</th>
			<th rowspan="2">درست</th>
			<th rowspan="2">نادرست</th>
			<th rowspan="2">نزده</th>
			<th rowspan="2">درصد</th>
			<th rowspan="2">نمره تراز</th>
			<th rowspan="2">وضعیت</th>
			<th colspan="3">رتبه</th>
			<th rowspan="2">میانگین درصد نفرات برتر</th>
			<th rowspan="2">میانگین درصد کل</th>
		</tr>
		<tr>
			<th>کلاسی</th>
			<th>مدرسه</th>
			<th>کیش</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>علوم</td>
			<td>{olom_count}</td>
			<td>{olom_true}</td>
			<td>{olom_false}</td>
			<td>{olom_empty}</td>
			<td>{olom_percent}</td>
			<td>{olom_taraz}</td>
			<td>{olom_status}</td>
			<td>{olom_rank_class}</td>
			<td>{olom_rank_level}</td>
			<td>-</td>
			<td>{olom_5avg}</td>
			<td>{olom_avg}</td>
		</tr>
	</tbody>
	<tbody>
		<tr>
			<td>مطالعات اجتماعی</td>
			<td>{motaleat_count}</td>
			<td>{motaleat_true}</td>
			<td>{motaleat_false}</td>
			<td>{motaleat_empty}</td>
			<td>{motaleat_percent}</td>
			<td>{motaleat_taraz}</td>
			<td>{motaleat_status}</td>
			<td>{motaleat_rank_class}</td>
			<td>{motaleat_rank_level}</td>
			<td>-</td>
			<td>{motaleat_5avg}</td>
			<td>{motaleat_avg}</td>
		</tr>
	</tbody>
	<tbody>
		<tr>
			<td>زبان انگیسی</td>
			<td>{zaban_count}</td>
			<td>{zaban_true}</td>
			<td>{zaban_false}</td>
			<td>{zaban_empty}</td>
			<td>{zaban_percent}</td>
			<td>{zaban_taraz}</td>
			<td>{zaban_status}</td>
			<td>{zaban_rank_class}</td>
			<td>{zaban_rank_level}</td>
			<td>-</td>
			<td>{zaban_5avg}</td>
			<td>{zaban_avg}</td>
		</tr>
	</tbody>
	<tbody>
		<tr>
			<td>ریاضی</td>
			<td>{riazi_count}</td>
			<td>{riazi_true}</td>
			<td>{riazi_false}</td>
			<td>{riazi_empty}</td>
			<td>{riazi_percent}</td>
			<td>{riazi_taraz}</td>
			<td>{riazi_status}</td>
			<td>{riazi_rank_class}</td>
			<td>{riazi_rank_level}</td>
			<td>-</td>
			<td>{riazi_5avg}</td>
			<td>{riazi_avg}</td>
		</tr>
	</tbody>
	<tbody>
		<tr>
			<td>فارسی</td>
			<td>{farsi_count}</td>
			<td>{farsi_true}</td>
			<td>{farsi_false}</td>
			<td>{farsi_empty}</td>
			<td>{farsi_percent}</td>
			<td>{farsi_taraz}</td>
			<td>{farsi_status}</td>
			<td>{farsi_rank_class}</td>
			<td>{farsi_rank_level}</td>
			<td>-</td>
			<td>{farsi_5avg}</td>
			<td>{farsi_avg}</td>
		</tr>
	</tbody>
	<tbody>
		<tr>
			<td>عربی</td>
			<td>{arabi_count}</td>
			<td>{arabi_true}</td>
			<td>{arabi_false}</td>
			<td>{arabi_empty}</td>
			<td>{arabi_percent}</td>
			<td>{arabi_taraz}</td>
			<td>{arabi_status}</td>
			<td>{arabi_rank_class}</td>
			<td>{arabi_rank_level}</td>
			<td>-</td>
			<td>{arabi_5avg}</td>
			<td>{arabi_avg}</td>
		</tr>
	</tbody>
	<tbody>
		<tr>
			<td>پیام های آسمانی و قرآن</td>
			<td>{qoran_count}</td>
			<td>{qoran_true}</td>
			<td>{qoran_false}</td>
			<td>{qoran_empty}</td>
			<td>{qoran_percent}</td>
			<td>{qoran_taraz}</td>
			<td>{qoran_status}</td>
			<td>{qoran_rank_class}</td>
			<td>{qoran_rank_level}</td>
			<td>-</td>
			<td>{qoran_5avg}</td>
			<td>{qoran_avg}</td>
		</tr>
	</tbody>
</table>
                            	
							</div>

                        </div>

                        <div class="block-flat">
                            <div class="header">
                                <h4><i class="glyphicon glyphicon-stats"></i> پاسخنامه </h4>
                            </div>
                            <div class="content">

<table class="answer-table">
<!--answer1-->
<tr>
	<td class="{D}"></td>
	<td class="{C}"></td>
	<td class="{B}"></td>
	<td class="{A}"></td>
	<td class="count">:{count}</td>
</tr>
<!--/answer1-->
</table>
<table class="answer-table">
<!--answer2-->
<tr>
	<td class="{D}"></td>
	<td class="{C}"></td>
	<td class="{B}"></td>
	<td class="{A}"></td>
	<td class="count">:{count}</td>
</tr>
<!--/answer2-->
</table>
<table class="answer-table">
<!--answer3-->
<tr>
	<td class="{D}"></td>
	<td class="{C}"></td>
	<td class="{B}"></td>
	<td class="{A}"></td>
	<td class="count">:{count}</td>
</tr>
<!--/answer3-->
</table>
<table class="answer-table">
<!--answer4-->
<tr>
	<td class="{D}"></td>
	<td class="{C}"></td>
	<td class="{B}"></td>
	<td class="{A}"></td>
	<td class="count">:{count}</td>
</tr>
<!--/answer4-->
</table>
<table class="answer-table">
<!--answer5-->
<tr>
	<td class="{D}"></td>
	<td class="{C}"></td>
	<td class="{B}"></td>
	<td class="{A}"></td>
	<td class="count">:{count}</td>
</tr>
<!--/answer5-->
</table>
<table class="answer-table">
<!--answer6-->
<tr>
	<td class="{D}"></td>
	<td class="{C}"></td>
	<td class="{B}"></td>
	<td class="{A}"></td>
	<td class="count">:{count}</td>
</tr>
<!--/answer6-->
</table>
<div class="clearfix"></div>

<br>
<ol>
	<li><div class="fill answer glyphicon glyphicon-ok answer-table-info"></div> درست </li>
	<li><div class="fill glyphicon answer-table-info"></div> نادرست</li>
	<li><div class="empty glyphicon answer-table-info"></div> نزده</li>
	<li><div class="answer glyphicon glyphicon-ok answer-table-info"></div>
		<div class="empty answer glyphicon glyphicon-ok answer-table-info"></div>
		پاسخ های درست 
		</li>
</ol>





							</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NedaArea END -->
        </div>
    </div>


    <!-- ourBody END -->
    <!-- Our Default modal template - END -->

    <!-- START Sripts -->
    <script type="text/javascript" src="{static_path}/lib/jquery.min.js"></script>
    <script type="text/javascript" src="{static_path}/js/cleanzone.js"></script>
    <script src="{static_path}/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="{static_path}/lib/jquery.gritter/jquery.gritter.min.js" type="text/javascript"></script>
    <script src="{static_path}/lib/jquery.parsley/parsley.remote.min.js" type="text/javascript"></script>
    <script src="{static_path}/lib/jquery.parsley/parsley.config.js" type="text/javascript"></script>
    <script src="{static_path}/js/forms.js" type="text/javascript"></script>
    <script src="{static_path}/js/student.js" type="text/javascript"></script>
    <script type="text/javascript">
        App.init();
    </script>
    <!-- END Sripts -->
</body>

</html>