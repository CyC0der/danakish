<?php

/* open database and add to the registry */
$MySQL = new mysqli('localhost', 'root', '', 'danakish');

/* check database connection */
if ($MySQL->connect_error)
	throw new Exception('Connect Error (' . $MySQL->connect_errno . ') ' . $MySQL->connect_error);

if (!$MySQL->set_charset("utf8"))
	throw new Exception("Error loading character set utf8: " . $MySQL->error);

function proTrim($str) {
	
	$str = explode(' ',$str);
	
	$out = array();
	
	$len = count($str);
	
	for($i=0;$i<$len;$i++)
		$out[] = trim($str[$i]);
	
	return strtr(trim(implode('',$out)),
	array(
		'ی'=>'',
		'ي'=>''
	));
}

/*
$list = $MySQL->query('SELECT * FROM `student_info` ORDER BY `name` ASC;')->fetch_all();
$list2 = $list2org = array();
foreach($list as $name) {
	$list2[] = proTrim($name[6]);
	$list2org[] = $name[6];
}
	
$qy = $MySQL->query('SELECT * FROM `students` ORDER BY `students`.`firstname` ,`students`.`lastname` ASC;');

$j = 0;

while($row = $qy->fetch_assoc()) {
	

	 $list1 = proTrim(trim($row[firstname]) . ' ' .  trim($row[lastname]));
	 
	 $searchedIndex = array_search($list1,$list2);
	if($searchedIndex !== false) {
			echo $list2org[$searchedIndex] . ' <br>';
		$MySQL->query('UPDATE `student_info` SET sn="'.$row[sn].'" WHERE(name LIKE "'.$list2org[$searchedIndex].'");');
	}

}*/

/*
$qy = $MySQL->query('SELECT * FROM `students`;');
$i=1;
while($row = $qy->fetch_assoc()) {
	$name = proTrim(trim($row[firstname]) . trim($row[lastname]));
	$MySQL->query('UPDATE `student_info` SET sn="'.$row[sn].'" WHERE MATCH (name) AGAINST ("'.$name.'");');
	echo $i. ' ' .$name . ' ' . $MySQL->affected_rows . '<br>';
	
	$i++;
}*/

$qy = $MySQL->query('SELECT * FROM `students`;');
$i=1;
while($row = $qy->fetch_assoc()) {
	$name = proTrim(trim($row[firstname]) . trim($row[lastname]));
	
	$len = mb_strlen($name,'utf-8');
	//$substr = mb_substr($name,NULL ,$len/2 - $len,'utf-8');
	$substr = mb_substr($name,NULL ,$len/2,'utf-8');
	$MySQL->query('UPDATE `student_info` SET sn="'.$row[sn].'" WHERE sn="" AND name LIKE "%'.$substr .'%";');
	echo $i. ' ' .$name . ' substr:'.$substr.' ' . $MySQL->affected_rows . '<br>';
	
	$i++;
}


/*UPDATE students LEFT JOIN student_info
ON students.sn = student_info.sn
SET students.mother_cell = student_info.mother_cell,
students.father_cell = student_info.father_cell

SELECT * FROM `students` WHERE `parent_cell`="" AND `father_cell`="" AND  `mother_cell`=""
*/

?>