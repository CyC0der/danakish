<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

/*20 آدرس فایل تصویر منبع */
$templateName = 'workbook-sample.jpg';
/*20 فونت کلی سیستم*/
const font = 'gdfarsi/fonts/KOODAKB.TTF';

const en_font = 'gdfarsi/fonts/ARLRDBD.TTF';

/*20 فونت یوزرنیم و پسورد*/
const code_font = 'gdfarsi/fonts/TerminusTTF-4.39.ttf';
/*20 گام افزایشی بین سطر های جدول نمرات*/
$top  = 150;
/*20 گام افزایشی بین سطر های پاسخنامه */
const SHEET_COLUMN_TOP = 50;


require_once  'gdfarsi/FarsiGD.php';

	
/*20 مشخصات دیتابیس */
$MySQL = new mysqli('localhost', 'root', '', 'danakish');

/* check database connection */
if ($MySQL->connect_error)
	throw new Exception('Connect Error (' . $MySQL->connect_errno . ') ' . $MySQL->connect_error);

if (!$MySQL->set_charset("utf8"))
	throw new Exception("Error loading character set utf8: " . $MySQL->error);

$gd = new FarsiGD();


echo "\r\n:) making workbook for level 7: \r\n\r\n";

$qy = $MySQL->query('SELECT * FROM students 
	INNER JOIN exams using(level) 
	ORDER BY 
	`students`.`level` ASC,
	`students`.`class` ASC,
	`students`.`lastname` ASC,
	`students`.`firstname` ASC;');
$count = 1;
while($student=$qy->fetch_assoc()) {
	
	$image = @imagecreatefromjpeg($templateName);
	if(!$image) {
		echo 'invalid JPG template fileName: ' . $templateName;
		sleep(5);
		exit;
	}
	
	$data = prepareData($student[sn],$student[level],$student['class'],$student[exams_id],$MySQL);
	
	$black = imagecolorallocate($image , 0, 0, 0);
	
	$strLevel = array('7'=>'پایه هفتم','8'=>'پایه هشتم','9'=>'پایه نهم');
	
	$studentInfo =  $student[lastname] . '  ' . $student[firstname] . '  -  ' . $strLevel[$student[level]];
	
	/* $20 : نام ، نام خانوادگی پایه    */
	imagettftext($image, 40, 0, 1750, 270 , $black , font,$gd->persianText($studentInfo  , 'fa', 'normal'));
	/* $20 : تراز کل    */
	imagettftext($image, 40, 0, 1083, 270, $black , font,$gd->persianText($data[all_avg]  , 'fa', 'normal'));
	/* $20 : سطح علمی کل   */
	imagettftext($image, 40, 0, 390, 270, $black , font,$gd->persianText(calcStatus($data[all_avg] )  , 'fa', 'normal'));
	/* $20 : نام کاربری   */
	imagettftext($image, 40, 0, 160, 385, $black , code_font,$student[sn]);
	/* $20 : رمزعبور   */
	imagettftext($image, 40, 0, 160, 445, $black , code_font,$student[password]);
	
	writeLine($image,$gd,$top*0,'olom',$data);
	writeLine($image,$gd,$top*1,'motaleat',$data);
	writeLine($image,$gd,$top*2,'zaban',$data);
	writeLine($image,$gd,$top*3,'riazi',$data);
	writeLine($image,$gd,$top*4,'farsi',$data);
	writeLine($image,$gd,$top*5,'arabi',$data);
	writeLine($image,$gd,$top*6,'qoran',$data);
	
	writeSheetAnswer($image,$data[answer],$data[answer_main],$gd);
	
	$data = NULL;

	$fileName = $student['level'] . $student['class'] . str_pad($count, 3, "0", STR_PAD_LEFT);
	
	if(imagejpeg($image, "workbooks/".$fileName.".jpg")) {
			echo "workbooks/".$fileName.".jpg for ".$student[sn]." created successful \r\n";
		} else {
			echo "ERROR! workbooks/".$fileName.".jpg for ".$student[sn]." not created! \r\n";
	}
		
	imagedestroy($image);
	$count++;
}

function prepareData($student_id,$level,$class,$exams_id,$MySQL) {
	
	$exp = array();
	
	$examInfo = $MySQL->query('SELECT 
		`answer`, 
		`olom_avg` as olom, 
		`motaleat_avg` as motaleat, 
		`zaban_avg` as zaban, 
		`riazi_avg` as riazi,
		`farsi_avg` as farsi, 
		`arabi_avg` as arabi, 
		`qoran_avg` as qoran
		FROM `exams` 
		WHERE(`exams_id`="'.$exams_id.'");'
	)->fetch_assoc();
	
	
	
	$exp[percent_avg] = $examInfo;
	
	$exp[answer_main] = $examInfo[answer];
	
	$examInfo = NULL;
	
	$points_table = $MySQL->query('SELECT 
		code,answer,scores,
		`olom_taraz` as olom, 
		`motaleat_taraz` as motaleat, 
		`zaban_taraz` as zaban, 
		`riazi_taraz` as riazi, 
		`farsi_taraz` as farsi, 
		`arabi_taraz` as arabi,
		`qoran_taraz` as qoran
		FROM 
		`points` 
		WHERE(
			`exams_id`="'.$exams_id.'" AND 
			sn="'.$student_id.'");'
	)->fetch_assoc();
	
	$exp[taraz] = $points_table;
	unset($exp[taraz][code],$exp[taraz][answer],$exp[taraz][scores]);
	
	
	
	$exp[rank_level] = $MySQL->query('SELECT  
		FIND_IN_SET(`olom_taraz`, 
			(SELECT GROUP_CONCAT( `olom_taraz` ORDER BY `olom_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
		) AS olom,
		FIND_IN_SET(`motaleat_taraz`, 
			(SELECT GROUP_CONCAT( `motaleat_taraz` ORDER BY `motaleat_taraz` DESC ) FROM `points`
			WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
		) AS motaleat,
		FIND_IN_SET(`zaban_taraz`,
			(SELECT GROUP_CONCAT( `zaban_taraz` ORDER BY `zaban_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
		) AS zaban,
		FIND_IN_SET(`riazi_taraz`, 
			(SELECT GROUP_CONCAT( `riazi_taraz` ORDER BY `riazi_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
		) AS riazi,
		FIND_IN_SET(`farsi_taraz`, 
			(SELECT GROUP_CONCAT( `farsi_taraz` ORDER BY `farsi_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
		) AS farsi,
		FIND_IN_SET(`arabi_taraz`, 
			(SELECT GROUP_CONCAT( `arabi_taraz` ORDER BY `arabi_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
		) AS arabi,
		FIND_IN_SET(`qoran_taraz`, 
			(SELECT GROUP_CONCAT( `qoran_taraz` ORDER BY `qoran_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND `exams_id`="'.$exams_id.'"))
		) AS qoran
		FROM `points`
		WHERE(sn =  "'.$student_id.'");'
	)->fetch_assoc();

	$exp[rank_class] = $MySQL->query('SELECT  
		FIND_IN_SET(`olom_taraz`, 
			(SELECT GROUP_CONCAT( `olom_taraz` ORDER BY `olom_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
		) AS olom,
		FIND_IN_SET(`motaleat_taraz`, 
			(SELECT GROUP_CONCAT( `motaleat_taraz` ORDER BY `motaleat_taraz` DESC ) FROM `points`
			WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
		) AS motaleat,
		FIND_IN_SET(`zaban_taraz`,
			(SELECT GROUP_CONCAT( `zaban_taraz` ORDER BY `zaban_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
		) AS zaban,
		FIND_IN_SET(`riazi_taraz`, 
			(SELECT GROUP_CONCAT( `riazi_taraz` ORDER BY `riazi_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
		) AS riazi,
		FIND_IN_SET(`farsi_taraz`, 
			(SELECT GROUP_CONCAT( `farsi_taraz` ORDER BY `farsi_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
		) AS farsi,
		FIND_IN_SET(`arabi_taraz`, 
			(SELECT GROUP_CONCAT( `arabi_taraz` ORDER BY `arabi_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
		) AS arabi,
		FIND_IN_SET(`qoran_taraz`, 
			(SELECT GROUP_CONCAT( `qoran_taraz` ORDER BY `qoran_taraz` DESC ) FROM `points` 
			WHERE(level='.$level.' AND class='.$class.' AND `exams_id`="'.$exams_id.'"))
		) AS qoran
		FROM `points`
		WHERE(sn =  "'.$student_id.'");'
	)->fetch_assoc();
	
	$exp[avg5] = $MySQL->query('SELECT
		
			(SELECT ROUND(AVG(olom),2) 
				FROM (SELECT olom FROM `points` WHERE(level='.$level.')  ORDER BY `olom_taraz` DESC LIMIT 0,5) a1
			) as olom ,
			
			(SELECT ROUND(AVG(motaleat),2) 
				FROM (SELECT motaleat FROM `points` WHERE(level='.$level.')  ORDER BY `motaleat_taraz` DESC LIMIT 0,5) a2
			) as motaleat ,
			
			(SELECT ROUND(AVG(zaban),2) 
				FROM (SELECT zaban FROM `points` WHERE(level='.$level.')  ORDER BY `zaban_taraz` DESC LIMIT 0,5) a2
			) as zaban ,
			
			(SELECT ROUND(AVG(riazi),2) 
				FROM (SELECT riazi FROM `points` WHERE(level='.$level.')  ORDER BY `riazi_taraz` DESC LIMIT 0,5) a3
			) as riazi ,
			
			(SELECT ROUND(AVG(farsi),2) 
				FROM (SELECT farsi FROM `points` WHERE(level='.$level.')  ORDER BY `farsi_taraz` DESC LIMIT 0,5) a3
			) as farsi ,
			
			(SELECT ROUND(AVG(arabi),2) 
				FROM (SELECT arabi FROM `points` WHERE(level='.$level.')  ORDER BY `arabi_taraz` DESC LIMIT 0,5) a3
			) as arabi ,
			
			(SELECT ROUND(AVG(qoran),2) 
				FROM (SELECT qoran FROM `points` WHERE(level='.$level.')  ORDER BY `qoran_taraz` DESC LIMIT 0,5) a3
			) as qoran;'
	)->fetch_assoc();
	
	$exp[status] = array(
		'olom' => calcStatus($points_table['olom']),
		'motaleat'  => calcStatus($points_table['motaleat']), 
		'zaban'  => calcStatus($points_table['zaban']),
		'riazi'  => calcStatus($points_table['riazi']), 
		'farsi'  => calcStatus($points_table['farsi']),
		'arabi'  => calcStatus($points_table['arabi']),
		'qoran'  => calcStatus($points_table['qoran'])
	);
		
	$exp['count'] = getData();
	
	$exp[all_avg] = round((($points_table['olom']*2) +
		($points_table['motaleat']*2) + 
		($points_table['zaban']) + 
		($points_table['riazi']*3) + 
		($points_table['farsi']*2) + 
		($points_table['arabi']) + 
		($points_table['qoran']*2)) / 13);

	$exp[scores] = json_decode($points_table['scores'], true);
	
	$exp[answer] = $points_table['code'];
	
	
	
	return $exp;
}

function getData(){
	return array(
		'olom'=>20,
		'motaleat'=>10,
		'zaban'=>7,
		'riazi'=>25,
		'farsi'=>10,
		'arabi'=>8,
		'qoran'=>10
	);
}
	
function calcStatus($n){
	if($n >= 6000) {
		return 'عالی';
	} else if($n > 5300 && $n < 6000) {
		return 'بالای متوسط';
	} else if($n > 5000 && $n <= 5300) {
		return 'متوسط';
	} else if($n > 4200 && $n < 5000) {
		return 'زیر متوسط';
	} else if($n < 4200) {
		return 'نیازمند تلاش';
	}
}

function writeLine($image,$gd,$top,$skill,$data) {
	
	$black = imagecolorallocate($image , 0, 0, 0);

	/* $20 : تعداد سوال */
	imagettftext($image, 30, 0, 2010, 850 + $top, $black , font,$gd->persianText($data['count'][$skill]  , 'fa', 'normal'));
	/* $20 : تعداد درست */
	imagettftext($image, 30, 0, 1880, 850 + $top, $black , font,$gd->persianText($data['scores']['true'][$skill.'_true']  , 'fa', 'normal'));
	/* $20 : تعداد غلط */
	imagettftext($image, 30, 0, 1740, 850 + $top, $black , font,
		$gd->persianText($data['scores']['false'][$skill.'_false']  , 'fa', 'normal')
	);
	/* $20 : تعداد نزده */
	imagettftext($image, 30, 0, 1600, 850 + $top, $black , font,
		$gd->persianText($data['scores']['empty'][$skill.'_empty']  , 'fa', 'normal')
	);
	/* $20 : درصد */
	imagettftext($image, 25, 0, 1440, 850 + $top, $black ,en_font,
		$data['scores']['percent'][$skill.'_percent']
	);
	/* $20 : تراز */
	imagettftext($image, 30, 0, 1290, 850 + $top, $black , font,$gd->persianText($data['taraz'][$skill]  , 'fa', 'normal'));
	/* $20 : وضعیت */
	imagettftext($image, 30, 0, 1035, 850 + $top, $black , font,$gd->persianText($data['status'][$skill]  , 'fa', 'normal'));
	/* $20 : رتبه کلاس */
	imagettftext($image, 30, 0, 910, 850 + $top, $black , font,$gd->persianText($data['rank_class'][$skill]  , 'fa', 'normal'));
	/* $20 : رتبه مدرسه */
	imagettftext($image, 30, 0, 770, 850 + $top, $black , font,$gd->persianText($data['rank_level'][$skill]  , 'fa', 'normal'));
	/* $20 : میانگین درصد نفرات برتر */
	imagettftext($image, 25, 0, 400, 850 + $top, $black , en_font,$data['avg5'][$skill]);
	/* $20 : میانگین درصد کل */
	imagettftext($image, 25, 0, 120, 850 + $top, $black , en_font,$data['percent_avg'][$skill]);
}


function writeSheetAnswer($image,$answer,$main_answer,$gd) {
	
	$answer = json_decode($answer, true);	
	
	$main_answer = json_decode($main_answer, true);	
	
	$black = imagecolorallocate($image , 0, 0, 0);
	
	$c = array('A'=>1,'B'=>2,'C'=>3,'D'=>4);
	
	for($i=1;$i<=30;$i++){

		//$answer[$i] = (empty(trim($answer[$i]))?'EMPTY':$answer[$i]);
		
		/* $20 : پاسخنامه ، پاسخ صحیح سطر یک تا سی   */
		imagettftext($image, 30, 0, 640, 1975 + ($i*SHEET_COLUMN_TOP), $black , font,
			$gd->persianText($c[$main_answer[$i]]  , 'fa', 'normal')
		);
		
		/* $20 : پاسخنامه ، پاسخ شما سطر یک تا سی   */
		imagettftext($image, 30, 0, 707, 1975 + ($i*SHEET_COLUMN_TOP), $black , font,$gd->persianText($c[$answer[$i]]  , 'fa', 'normal'));
	}
	
	$j=1;
	for($i=31;$i<=60;$i++){
		
		//$answer[$i] = (empty(trim($answer[$i]))?'EMPTY':$answer[$i]);
		
		/* $20 : پاسخنامه ، پاسخ صحیح سطر سی تا شصت   */
		imagettftext($image, 30, 0, 1383, 1975 + ($j*SHEET_COLUMN_TOP), $black , font,
			$gd->persianText($c[$main_answer[$i]]  , 'fa', 'normal')
		);
		
		/* $20 : پاسخنامه ، پاسخ شما سطر  سی تا شصت   */
		imagettftext($image, 30, 0, 1460, 1975 + ($j*SHEET_COLUMN_TOP), $black , font,$gd->persianText($c[$answer[$i]]  , 'fa', 'normal'));
		$j++;
	}
	
	$j=1;
	for($i=61;$i<=90;$i++){
		
		//$answer[$i] = (empty(trim($answer[$i]))?'EMPTY':$answer[$i]);
		
		/* $20 : پاسخنامه ، پاسخ صحیح شصت سی تا نود   */
		imagettftext($image, 30, 0, 2130, 1975 + ($j*SHEET_COLUMN_TOP), $black , font,
			$gd->persianText($c[$main_answer[$i]]  , 'fa', 'normal')
		);
		
		/* $20 : پاسخنامه ، پاسخ شما سطر  شصت تا نود   */
		imagettftext($image, 30, 0, 2210, 1975 + ($j*SHEET_COLUMN_TOP), $black , font,$gd->persianText($c[$answer[$i]]  , 'fa', 'normal'));
		$j++;
	}
	
	
	/* خسته نباشی ;) */
		
}
?>