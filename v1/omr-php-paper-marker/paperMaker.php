<?php
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

	require_once  'gdfarsi/FarsiGD.php';
	require_once 'excel_reader2.php';

	
	/* تنظیمات */
	
	$title = array(
		'7'=>  'آزمون جامع پایه هفتم مرحله اول   15/9/1394',
		'8'=>  'آزمون جامع پایه هشتم مرحله اول   15/9/1394',
		'9'=> 'آزمون جامع پایه نهم مرحله اول   15/9/1394'
	);

	
	$font = 'gdfarsi/fonts/KOODAKB.TTF';
	
	$templateName = 'tpl.jpg';
	
	/* تنظیمات باکس کد دانش آموزی */
	
	$top_leftX = 361;
	$top_leftY = 703;
	$cycleSize = 40;
	$col = 8;
	$row = 20.5;

	
	$files = scandir('pics');

	foreach($files as $name) {

		if(is_file('pics/' . $name)) {
			$file = basename($name, ".jpg");
			if(strlen($file)!=10) {
				$new = str_pad($file, 10, "0", STR_PAD_LEFT) . '.jpg';
				if(rename('pics/' . $name, 'pics/' . $new)) {
					echo 'file pics/'.$name.' renamed to '.$new." successfully!\r\n";
				} else {
					echo 'ERROR! file Rename! pics/'.$name . "\r\n";
				}
			}
		}
	}


	$data = new Spreadsheet_Excel_Reader("list.xls");

	$gd = new FarsiGD();
	
	$rowNum =  $data->rowcount();
	
	for($j=1;$j <= $rowNum;$j++) {
		
		 $im = @imagecreatefromjpeg($templateName);
		 
		 if(!$im) {
			echo 'آدرس فایل تمپلیت اشتباه است';
			exit;
		 }
		 
		$black = imagecolorallocate($im , 0, 0, 0);

		$sn = str_pad((string)$data->val($j,'A'), 10, "0", STR_PAD_LEFT);
		
		for($i=0;$i<=9;$i++) {
			
			imagefilledellipse( $im, $top_leftX+($i*($row+$cycleSize)), $top_leftY+($sn[$i]*($col+$cycleSize)), $cycleSize, $cycleSize, $black);
		}
		
		
		$photo = @imagecreatefromjpeg('pics/'.$sn.'.jpg');
		if(!$photo) {
			$photo = @imagecreatefromjpeg('default.jpg');
		}
		
		imagecopy($im, $photo, 1802, 536, 0, 0, 306, 389);
		
		$red = imagecolorallocate($im ,255, 0, 0);
		imagettftext($im, 70, 0, 1650, 450, $red , $font,$gd->persianText($data->val($j,'F') .' :شماره صندلی'  , 'fa', 'normal'));
		
		imagettftext($im, 35, 0, 1070, 710, $black, $font, $gd->persianText($data->val($j,'B'), 'fa', 'normal'));
		imagettftext($im, 35, 0, 1355, 710, $black, $font, $gd->persianText($data->val($j,'C'), 'fa', 'normal'));
		imagettftext($im, 35, 0, 1070, 830, $black, $font, $gd->persianText( $sn ."   : شماره دانش آموزی" , 'fa', 'normal'));
		imagettftext($im, 35, 0, 1000, 580, $black, $font, $gd->persianText($title[$data->val($j,'E')], 'fa', 'normal'));

		$number = str_pad((string)$data->val($j,'F'), 3, "0", STR_PAD_LEFT);
		
		if(imagejpeg($im, "export/".$number.".jpg")) {
			echo 'ID: ' . $number ." In export/".$number.".jpg for ".$sn." created successful \r\n";
		} else {
			echo 'ERROR! ID: ' . $number ." In export/".$number.".jpg for ".$sn." not created! \r\n";
		}
	
		imagedestroy($im);
	}
?>